//
//  LoadingScreen.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 1/2/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingScreen : UIView

-(void)loadingScreen:(void(^)(void))loading;
-(void)finishLoading;
@end
