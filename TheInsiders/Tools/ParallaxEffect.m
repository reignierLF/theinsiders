//
//  ParallaxEffect.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 12/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "ParallaxEffect.h"

@implementation ParallaxEffect

-(instancetype)init{

    self = [super init];
    
    if(self){
    
    }
    
    return self;
}

-(void)parallax:(UIImageView*)imageView intensity:(NSInteger)intensity{
    
    /*
     * Set vertical effect
     */
    UIInterpolatingMotionEffect *verticalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-intensity);
    verticalMotionEffect.maximumRelativeValue = @(intensity);
    
    /*
     * Set horizontal effect
     */
    UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-intensity);
    horizontalMotionEffect.maximumRelativeValue = @(intensity);
    
    /*
     * Create group to combine both
     */
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    /*
     * Add both effects to your view
     */
    [imageView addMotionEffect:group];
}
@end
