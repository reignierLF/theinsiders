//
//  PickerView.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 5/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PickerView;

@protocol PickerViewDelegate

-(void)pickerViewDidSelectRow:(PickerView*)pickerView salutation:(NSString*)salutation;

@end


@interface PickerView : UIView

@property (nonatomic, strong) NSArray *dataArray;
//@property (nonatomic, strong) NSString *salutation;

@property (nonatomic, weak) id <PickerViewDelegate> delegate;

-(void)showPicker;
@end
