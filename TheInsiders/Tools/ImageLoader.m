//
//  ImageLoader.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ImageLoader.h"

@interface ImageLoader ()

@end

@implementation ImageLoader

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        
        _isDisplayErrorImage = YES;
    }
    
    return self;
}

-(void)parseImage:(UIImageView*)imageView url:(NSString*)urlString errorImageName:(NSString*)errorImageName style:(UIActivityIndicatorViewStyle)style{
    
    UIView *backgroundLoadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
    backgroundLoadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    [imageView addSubview:backgroundLoadingView];
    
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, backgroundLoadingView.frame.size.height / 2, backgroundLoadingView.frame.size.width - 40, 20)];
    //loadingLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    loadingLabel.text = @"Loading...";
    loadingLabel.textColor = [UIColor lightGrayColor];
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.font = [UIFont systemFontOfSize:14];
    //[backgroundLoadingView addSubview:loadingLabel];
    
    UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    loadingIndicator.frame = CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height - (imageView.frame.size.height / 4));
    [loadingIndicator setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [backgroundLoadingView addSubview:loadingIndicator];
    [loadingIndicator startAnimating];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        /*
         * Check if image url is null
         * If null = image is replace with no image background picture
         * If not null = image is loaded from url
         */
        
        __block UIImage *image;
        
        if ([urlString isEqualToString:errorImageName] || urlString.length == 0) {
            
            image = [UIImage imageNamed:errorImageName];
        }else{
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:urlString];
            
            if(data == nil){
                data = [NSData dataWithContentsOfURL:url];
                
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:urlString];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //NSLog(@"no image cache but make cache now");
            }else{
                //NSLog(@"image cache");
            }
            
            /*
             * Incase failed to finish the downloading the image
             * We replace the image to our no image background picture
             */
            
            if(data == nil){
                image = [UIImage imageNamed:errorImageName];
            }else{
                image = [UIImage imageWithData:data];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            /*
             * Set new image
             */
            
            UIImage * newImage = image;
            
            [UIView transitionWithView:imageView
                              duration:1.0f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                
                                backgroundLoadingView.alpha = 0.0;
                                
                                imageView.image = newImage;
                                
                                if(!_isDisplayErrorImage){
                           
                                    imageView.image = nil;
                                }
                            }completion:nil];
        });
    });
}
@end
