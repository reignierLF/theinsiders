//
//  ImageLoader.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageLoader : NSObject

@property (nonatomic) BOOL isDisplayErrorImage;

-(void)parseImage:(UIImageView*)imageView url:(NSString*)urlString errorImageName:(NSString*)errorImageName style:(UIActivityIndicatorViewStyle)style;

@end
