//
//  LoadingScreen.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 1/2/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "LoadingScreen.h"

@interface LoadingScreen ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) UIWindow *appWindow;
@property (nonatomic, strong) UIView *loadingView;
@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicator;

@end

@implementation LoadingScreen

-(instancetype)init{

    self = [super init];
    
    if(self){
    
        _appWindow = [UIApplication sharedApplication].keyWindow;
        
        _viewWidth = _appWindow.frame.size.width;
        _viewHeight = _appWindow.frame.size.height;
    }
    
    return self;
}

-(void)didMoveToSuperview{

    
}

-(void)loadingScreen:(void(^)(void))loading{
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        
        [_appWindow addSubview:self];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        _loadingView.alpha = 0.0;
        [_appWindow addSubview:_loadingView];
        
        _loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _loadingIndicator.frame = _loadingView.frame;
        [_loadingView addSubview:_loadingIndicator];
        [_loadingIndicator startAnimating];
        
        loading();
        
        [UIView animateWithDuration: 0.3
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations: ^{
                             
                             _loadingView.alpha = 1.0;
                         }completion:nil];
    });
}

-(void)finishLoading{
    
    [UIView animateWithDuration: 0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         _loadingView.alpha = 0.0;
                     }completion:^(BOOL complete){
                     
                         [_loadingIndicator stopAnimating];
                         [_loadingIndicator removeFromSuperview];
                         _loadingIndicator = nil;
                         
                         _loadingView.userInteractionEnabled = NO;
                         [_loadingView removeFromSuperview];
                         _loadingView = nil;
                     }];
}
@end
