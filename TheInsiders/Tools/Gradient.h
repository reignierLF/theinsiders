//
//  Gradient.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 7/2/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Gradient : UIColor

@property (nonatomic, strong) UIColor *gradient;

-(UIColor*)gradient;

@end
