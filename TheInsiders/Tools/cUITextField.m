//
//  cUITextField.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 23/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "cUITextField.h"

@implementation cUITextField

static CGFloat margin = 10;

- (CGRect)textRectForBounds:(CGRect)bounds
{
    bounds.origin.x += margin;
    
    bounds.size.width -= margin;
    
    return bounds;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    bounds.origin.x += margin;
    
    bounds.size.width -= margin;
    
    return bounds;
}

@end
