//
//  RsvpViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 6/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "RsvpViewController.h"
#import "Details.h"

#import "cUIScrollView.h"
#import "ParallaxEffect.h"
#import "ImageLoader.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "ViewInvitations.h"

@interface RsvpViewController () <UIScrollViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;

@property (nonatomic, strong) cUIScrollView *scrollView;
@property (nonatomic, strong) UILabel *fullNameLabel;

@property (nonatomic) NSInteger counter;

@property (nonatomic, strong) ParallaxEffect *pe;
@property (nonatomic, strong) ImageLoader *il;
@property (nonatomic, strong) LoadingScreen *ls;
@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;
@property (nonatomic, strong) ViewInvitations *vi;

@end

@implementation RsvpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    
    [_ls loadingScreen:^{
       
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api getViewInvitationsWithToken:_uc.authenticationToken eventId:_eventId isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"view invitation json = %@", jsonDictionary);
                
                _vi = [[ViewInvitations alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(!_vi.status){
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:_vi.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        }else{
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Screenshot this image and send it to your invited guests.\n\nSimply have them present the QR code at the registration booth for them to gain entry to the event." preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                            [self initBackgroundImage];
                            [self initRSVPDetails];
                            [self initRsvpItemDetails];
                            [self initRSVPBar];
                        }
                        
                        NSLog(@"success view invitation");
                        
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at view invitation");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{

    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Futura ICG" size:18] };
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.title = @"RSVP";
    
    _counter = 0;
    
    _pe = [[ParallaxEffect alloc] init];
    _il = [[ImageLoader alloc] init];
    _ls = [[LoadingScreen alloc] init];
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
}

-(void)initBackgroundImage{
    
    //UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:_eventImage];
    UIImageView *backgroundImageView = [[UIImageView alloc] init];
    backgroundImageView.frame = CGRectMake(-5, -5, _screenWidth + 10, _screenHeight + 10);
    //backgroundImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
    
    [_pe parallax:backgroundImageView intensity:20];
    
    NSLog(@"image url = %@",[_vi.eventImageUrl objectAtIndex:0]);
    
    [_il parseImage:backgroundImageView url:[_vi.eventImageUrl objectAtIndex:0] errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
}

-(void)initRSVPDetails{
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(40, _navigationBarHeight + 30, _screenWidth - 80, _screenHeight - ((_navigationBarHeight * 2) + 30))];
    _scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * (_vi.size + 2), _scrollView.frame.size.height);
    _scrollView.delegate = self;
    _scrollView.delaysContentTouches = NO;
    _scrollView.userInteractionEnabled = NO;
    [self.view addSubview:_scrollView];
}

-(void)initRsvpItemDetails{
    
    for (int i = 0; i < _vi.size; i++) {
        
        Details *details = [[Details alloc] initWithFrame:CGRectMake(_scrollView.frame.size.width * i, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
        details.qrCodeImageUrl = [_vi.qrCodeUrl objectAtIndex:i];
        details.fromPerson = [_vi.invitedBy objectAtIndex:i];
        details.nameOfEvent = [_vi.title objectAtIndex:i];
        details.dateOfEvent = [_vi.dateTime objectAtIndex:i];
        details.placeOfEvent = [_vi.address objectAtIndex:i];
        [_scrollView addSubview:details];
    }
    
    [_scrollView scrollRectToVisible:CGRectMake(0, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:NO];
}

-(void)initRSVPBar{

    UIView *rsvpBarView = [[UIView alloc] initWithFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, 40)];
    //rsvpBarView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:rsvpBarView];
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeSystem];
    previousButton.frame = CGRectMake(0, 0, rsvpBarView.frame.size.width / 4, rsvpBarView.frame.size.height);
    //previousButton.backgroundColor = [UIColor redColor];
    previousButton.tag = 0;
    [rsvpBarView addSubview:previousButton];
    
    [previousButton addTarget:self action:@selector(loopEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    float iconSize = 15;
    
    UIImageView *previousImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Left"]];
    previousImageView.frame = CGRectMake((previousButton.frame.size.width / 2) - (iconSize / 2), (previousButton.frame.size.height / 2) - (iconSize / 2), iconSize, iconSize);
    previousImageView.contentMode = UIViewContentModeScaleAspectFill;
    previousImageView.clipsToBounds = YES;
    previousImageView.userInteractionEnabled = NO;
    [previousButton addSubview:previousImageView];
    
    _fullNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(previousButton.frame.size.width, 0, rsvpBarView.frame.size.width / 2, rsvpBarView.frame.size.height)];
    //fullNameLabel.backgroundColor = [UIColor blueColor];
    _fullNameLabel.text = [NSString stringWithFormat:@"%@",[_vi.name objectAtIndex:_counter]];
    _fullNameLabel.textAlignment = NSTextAlignmentCenter;
    _fullNameLabel.textColor = [UIColor whiteColor];
    _fullNameLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [rsvpBarView addSubview:_fullNameLabel];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeSystem];
    nextButton.frame = CGRectMake(_fullNameLabel.frame.origin.x + _fullNameLabel.frame.size.width, 0, previousButton.frame.size.width, previousButton.frame.size.height);
    //nextButton.backgroundColor = [UIColor redColor];
    nextButton.tag = 1;
    [rsvpBarView addSubview:nextButton];
    
    [nextButton addTarget:self action:@selector(loopEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *nextImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Right"]];
    nextImageView.frame = CGRectMake((nextButton.frame.size.width / 2) - (iconSize / 2), (nextButton.frame.size.height / 2) - (iconSize / 2), iconSize, iconSize);
    nextImageView.contentMode = UIViewContentModeScaleAspectFill;
    nextImageView.clipsToBounds = YES;
    nextImageView.userInteractionEnabled = NO;
    [nextButton addSubview:nextImageView];
    
    NSLog(@"complete at rsvp bar");
}

-(void)loopEvent:(UIButton*)sender{
    
    NSLog(@"Before counter = %ld",(long)_counter);
    
    if(sender.tag == 0 && _counter > 0){
    
        _counter--;
        
        NSLog(@"previous QR code");
    }else if(sender.tag == 1 && _counter < (_vi.size - 1)){
        
        _counter++;
        
        NSLog(@"next QR code");
    }
    
    NSLog(@"After counter = %ld",(long)_counter);
    
    [_scrollView scrollRectToVisible:CGRectMake(_scrollView.frame.size.width * _counter, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:YES];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    if(_counter == _vi.size){
        
        _counter = _vi.size;
        
        NSLog(@"end");
        
        //[_scrollView scrollRectToVisible:CGRectMake(_scrollView.frame.size.width * _counter, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:NO];
    }else if(_counter == 0){
        
        _counter = 0;
        
        NSLog(@"beginning");
        
        //[_scrollView scrollRectToVisible:CGRectMake(_scrollView.frame.size.width * _counter, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:NO];
    }
    
    _fullNameLabel.text = [NSString stringWithFormat:@"%@",[_vi.name objectAtIndex:_counter]];
    
    NSLog(@"done scroll animation");
}
@end
