//
//  ForgotPasswordViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "ForgotPassword.h"

#import "cUITextField.h"

#import "Api.h"
#import "LoadingScreen.h"

@interface ForgotPasswordViewController () <UITextFieldDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) cUITextField *emailAddress;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) LoadingScreen *ls;

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden = NO;
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _api = [[Api alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
    //[self.view addSubview:_ls];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App BG"]];
    backgroundImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
    
    UIImageView *headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"The Insiders Hires Transp"]];
    headerImageView.frame = CGRectMake((_screenWidth / 8), 80, _screenWidth - ((_screenWidth / 8) * 2), 60);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    headerImageView.contentMode = UIViewContentModeScaleAspectFit;
    headerImageView.clipsToBounds = YES;
    [self.view addSubview:headerImageView];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake((_screenWidth / 12), headerImageView.frame.origin.y + headerImageView.frame.size.height + 10, _screenWidth - ((_screenWidth / 12) * 2), 100)];
    //headerLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
    headerLabel.text = @"Can't remember your password? \n\nType in your email address and we'll send\nyou directions on how to reset your password.";
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.numberOfLines = 0;
    //[headerLabel sizeToFit];
    [self.view addSubview:headerLabel];
    
    _emailAddress = [[cUITextField alloc] initWithFrame:CGRectMake(20, headerLabel.frame.origin.y + headerLabel.frame.size.height, _screenWidth - 40, 30)];
    _emailAddress.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    //_emailAddress.text = @"Email Address";
    //_emailAddress.placeholder = @"Email Address";
    _emailAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _emailAddress.textColor = [UIColor whiteColor];
    _emailAddress.layer.cornerRadius = 3;
    _emailAddress.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _emailAddress.delegate = self;
    [self.view addSubview:_emailAddress];
    
    UIButton *submit = [UIButton buttonWithType:UIButtonTypeSystem];
    submit.frame = CGRectMake(_emailAddress.frame.origin.x, _emailAddress.frame.origin.y + _emailAddress.frame.size.height + 6, _emailAddress.frame.size.width, 50);
    submit.layer.cornerRadius = 3;
    submit.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    submit.layer.borderWidth = 2;
    submit.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [submit setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [submit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:submit];
    
    [submit addTarget:self action:@selector(submitEvent) forControlEvents:UIControlEventTouchUpInside];
}

-(void)submitEvent{

    [_emailAddress resignFirstResponder];
    
    [_ls loadingScreen:^{
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [_api forgotPasswordWithEmail:_emailAddress.text isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"forgot password json = %@", jsonDictionary);
                
                ForgotPassword *fp = [[ForgotPassword alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:fp.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        
                        NSLog(@"success forgot password");
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at forgot password");
                    }
                    
                    [_ls finishLoading];
                    
                    _emailAddress.text = @"";
                });
            }];
        });
    }];
    
    NSLog(@"Submit forgot password");
}

/*
 * ==================== Delegates ====================
 */

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    /*
     * When the textfield was click
     * remove any inputs in the text field
     */
    
    //textField.text = @"";
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:0.25]}];
    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:1.0]}];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
    
    return YES;
}
@end
