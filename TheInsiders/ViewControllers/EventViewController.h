//
//  EventViewController.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 28/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventViewController : UIViewController

@property (nonatomic) NSInteger index;
@property (nonatomic, strong) NSString *eventId;
@property (nonatomic) BOOL isViewingOnly;
@property (nonatomic) BOOL isDisableInviteAndRSVP;

@end
