//
//  IceBreakerViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 9/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "IceBreakerViewController.h"
#import "RedeemedViewController.h"
#import "MenuController.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "Icebreaker.h"


@interface IceBreakerViewController () <MenuControllerDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;

@property (nonatomic, strong) MenuController *mc;
@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;
@property (nonatomic, strong) Icebreaker *ib;

@end

@implementation IceBreakerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackground];
    
    [_ls loadingScreen:^{
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api getIcebreakerWithToken:_uc.authenticationToken isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"icebreaker json = %@", jsonDictionary);
                
                _ib = [[Icebreaker alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                   
                    if(isComplete){
                    
                        [self initLayout];
                        
                        NSLog(@"success icebreaker");
                    }else{
                    
                        NSLog(@"failed to load json at icebreaker");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self initMenuController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{

    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    self.navigationController.navigationBarHidden = NO;
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
    //[self.view addSubview:_ls];
}

-(void)initBackground{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App Icebreake"]];
    backgroundImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initLayout{

    UIView *gradientView = [[UIView alloc] initWithFrame:CGRectMake(40, _navigationBarHeight + 30, _screenWidth - 80, (_screenHeight / 2) + 110)];
    //gradientView.alpha = 0.4;
    gradientView.layer.cornerRadius = 5;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = gradientView.bounds;
    gradient.cornerRadius = 5;
    gradient.colors = [NSArray arrayWithObjects:(id)[[[UIColor cyanColor] colorWithAlphaComponent:0.4] CGColor], (id)[[[UIColor purpleColor] colorWithAlphaComponent:0.4] CGColor], nil];
    [gradientView.layer insertSublayer:gradient atIndex:0];
    [self.view addSubview:gradientView];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, gradientView.frame.size.width - 40, 50)];
    //headerLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //headerLabel.text = @"Icebreaker";
    
    NSLog(@"title : %@",_ib.icebreakerTitle);
    NSLog(@"description : %@",_ib.icebreakerDescription);
    NSLog(@"%i",_ib.status);
    
    if(_ib.status){
        
        headerLabel.text = _ib.icebreakerTitle;
    }else{
        
        headerLabel.text = @"Icebreaker";
    }
    
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"Futura ICG" size:24];
    [gradientView addSubview:headerLabel];
    
    UILabel *bodyLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, headerLabel.frame.origin.y + headerLabel.frame.size.height + 10, gradientView.frame.size.width - 40, gradientView.frame.size.height - 170)];
    //bodyLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5];
    //bodyLabel.text = @"Make your way around the\nroom and look for the\nperson whose image\nmatches with yours.\n\nPresent both your phones at\nthe bar and redeem your free drinks!";
    
    if(_ib.status){
    
        bodyLabel.text = _ib.icebreakerDescription;
    }else{
    
        bodyLabel.text = _ib.message;
    }
    
    bodyLabel.textAlignment = NSTextAlignmentCenter;
    bodyLabel.numberOfLines = 0;
    bodyLabel.lineBreakMode = NSLineBreakByWordWrapping;
    bodyLabel.textColor = [UIColor whiteColor];
    bodyLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [gradientView addSubview:bodyLabel];
    
    if(_ib.status){
        
        UIButton *startButton = [UIButton buttonWithType:UIButtonTypeCustom];
        startButton.frame = CGRectMake(bodyLabel.frame.origin.x, bodyLabel.frame.origin.y + bodyLabel.frame.size.height + 10, bodyLabel.frame.size.width, 40);
        startButton.backgroundColor = [UIColor darkGrayColor];
        startButton.layer.cornerRadius = 5;
        [startButton setTitle:@"START" forState:UIControlStateNormal];
        [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [gradientView addSubview:startButton];
        
        [startButton addTarget:self action:@selector(startEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    //if(!_ib.status || !_ib.isRedeemed){
    
    //    startButton.backgroundColor = [UIColor grayColor];
    //    startButton.userInteractionEnabled = NO;
    //}
}

-(void)initMenuController{
    
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-(_screenWidth - (_screenWidth / 4)), 0, _screenWidth - (_screenWidth / 2.5), _screenHeight)];
    _mc.delegate = self;
    _mc.menu.settings.menuBackgroundImage = @"Insider App BG";
    _mc.menu.settings.textColor = [UIColor whiteColor];
    _mc.menu.settings.font = [UIFont fontWithName:@"Futura ICG" size:16];
    _mc.menu.settings.linebreakColor = [UIColor clearColor];
    _mc.popToFront = YES;
    _mc.headerSpace = 50;
    _mc.textMargin = 30;
    [self.view addSubview:_mc];
    
    UILabel *userFullNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 60, _mc.frame.size.width - 40, 50)];
    //userFullNameLabel.backgroundColor = [UIColor redColor];
    userFullNameLabel.text = [NSString stringWithFormat:@"%@",_uc.name];
    userFullNameLabel.textAlignment = NSTextAlignmentCenter;
    userFullNameLabel.textColor = [UIColor whiteColor];
    userFullNameLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [_mc addSubview:userFullNameLabel];
}

-(void)startEvent{
    
    RedeemedViewController *redeemedVC = [[RedeemedViewController alloc] init];
    redeemedVC.iceBreakerTitle = _ib.icebreakerTitle;
    redeemedVC.iceBreakerDesciption = _ib.icebreakerDescription;
    redeemedVC.iceBreakerImageUrl = _ib.icebreakerImageUrl;
    redeemedVC.isRedeemed = _ib.isRedeemed;
    [self.navigationController pushViewController:redeemedVC animated:YES];
}

-(void)didRefreshCurrentViewController{
    
}

-(void)didSelectMenu:(MenuController *)menuController viewController:(UIViewController *)viewController{
    
    NSLog(@"viewController = %@",viewController);
    
    if([viewController.title isEqualToString:@"Sign Out"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sign out" message:@"Are you sure you want to sign out?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* signout = [UIAlertAction actionWithTitle:@"Sign out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.view.clipsToBounds = YES;
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        [alertController addAction:signout];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{

        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self.navigationController pushViewController:viewController animated:NO];
            
            [UIView transitionFromView:self.view
                                toView:viewController.view
                              duration:0.25
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            completion:^(BOOL isComplete){
                                
                                [self garbageCollector:viewController];
                            }];
        });
    }
}

-(void)didSelectSubMenu:(MenuController *)menuController{
    
    NSLog(@"Did select sub menu");
}

-(void)garbageCollector:(UIViewController*)vc{
    
    _mc.menu.viewControllers = nil;
    [_mc.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _mc = nil;
    
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if(vc != nil){
        
        NSMutableArray *viewControllerArray = [self.navigationController.viewControllers mutableCopy];
        
        UIViewController *uv = [viewControllerArray objectAtIndex:1];
        
        [uv.view removeFromSuperview];
        uv = nil;
        
        [viewControllerArray removeObjectAtIndex:1];
        
        vc.navigationController.viewControllers = viewControllerArray;
    }
}
@end
