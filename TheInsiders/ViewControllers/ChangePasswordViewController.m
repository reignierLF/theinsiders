//
//  ChangePasswordViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "ChangePasswordViewController.h"

#import "cUITextField.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "ChangePassword.h"

@interface ChangePasswordViewController () <UITextFieldDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;

@property (nonatomic, strong) cUITextField *currentPassword;
@property (nonatomic, strong) cUITextField *passwordCreated;
@property (nonatomic, strong) cUITextField *passwordConfirmation;
@property (nonatomic, strong) UIButton *updatePassword;

@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //self.navigationController.navigationBarHidden = NO;
    self.title = @"Change Password";
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App BG"]];
    backgroundImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initLayout{

    _currentPassword = [[cUITextField alloc] initWithFrame:CGRectMake(20, _navigationBarHeight + 20, _screenWidth - 40, 30)];
    _currentPassword.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    //_currentPassword.text = @"Current Password";
    //_currentPassword.placeholder = @"Current Password";
    _currentPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Current Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _currentPassword.textColor = [UIColor whiteColor];
    _currentPassword.layer.cornerRadius = 3;
    _currentPassword.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _currentPassword.delegate = self;
    [self.view addSubview:_currentPassword];
    
    _passwordCreated = [[cUITextField alloc] initWithFrame:CGRectMake(_currentPassword.frame.origin.x, _currentPassword.frame.origin.y + _currentPassword.frame.size.height + 6, _currentPassword.frame.size.width, _currentPassword.frame.size.height)];
    _passwordCreated.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    //_passwordCreated.text = @"New Password";
    //_passwordCreated.placeholder = @"New Password";
    _passwordCreated.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _passwordCreated.textColor = [UIColor whiteColor];
    _passwordCreated.layer.cornerRadius = 3;
    _passwordCreated.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _passwordCreated.delegate = self;
    [self.view addSubview:_passwordCreated];
    
    _passwordConfirmation = [[cUITextField alloc] initWithFrame:CGRectMake(_passwordCreated.frame.origin.x, _passwordCreated.frame.origin.y + _passwordCreated.frame.size.height + 6, _passwordCreated.frame.size.width, _passwordCreated.frame.size.height)];
    _passwordConfirmation.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    //_passwordConfirmation.text = @"New Password Confirmation";
    //_passwordConfirmation.placeholder = @"New Password Confirmation";
    _passwordConfirmation.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password Confirmation" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _passwordConfirmation.textColor = [UIColor whiteColor];
    _passwordConfirmation.layer.cornerRadius = 3;
    _passwordConfirmation.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _passwordConfirmation.delegate = self;
    [self.view addSubview:_passwordConfirmation];
    
    _updatePassword = [UIButton buttonWithType:UIButtonTypeSystem];
    _updatePassword.frame = CGRectMake(_passwordConfirmation.frame.origin.x, _passwordConfirmation.frame.origin.y + _passwordConfirmation.frame.size.height + 6, _passwordConfirmation.frame.size.width, 50);
    _updatePassword.layer.cornerRadius = 3;
    _updatePassword.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    _updatePassword.layer.borderWidth = 2;
    _updatePassword.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_updatePassword setTitle:@"UPDATE PASSWORD" forState:UIControlStateNormal];
    [_updatePassword setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:_updatePassword];
    
    [_updatePassword addTarget:self action:@selector(updatePasswordEvent) forControlEvents:UIControlEventTouchUpInside];
}

-(void)updatePasswordEvent{

    [_currentPassword resignFirstResponder];
    [_passwordCreated resignFirstResponder];
    [_passwordConfirmation resignFirstResponder];
    
    if([_passwordCreated.text isEqualToString:_passwordConfirmation.text]){
    
        [_ls loadingScreen:^{
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                [_api changePaswordWithToken:_uc.authenticationToken currentPassword:_currentPassword.text newPassword:_passwordCreated.text isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                    
                    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                    
                    NSLog(@"change password json = %@", jsonDictionary);
                    
                    ChangePassword *cp = [[ChangePassword alloc] initWithDictionary:jsonDictionary];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        
                        if(isComplete){
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:cp.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                            
                            NSLog(@"success change password");
                        }else{
                            
                            NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                            NSLog(@"failed to load json at change password");
                        }
                        
                        [_ls finishLoading];
                    });
                }];
            });
        }];
    }else{
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Password not match" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:close];
        
        [self presentViewController:alertController animated:YES completion:nil];
    
        NSLog(@"password not match");
    }
}

/*
 * ==================== Delegates ====================
 */

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    /*
     * When the textfield was click
     * remove any inputs in the text field
     */
    
    //textField.text = @"";
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:0.25]}];
    
    /*
     * Detect if input is same to the word of "Password"
     * if YES/True, dont replace characters with *
     * if NO/False, replace all the characters with *
     */

    textField.secureTextEntry = YES;
    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:1.0]}];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
    
    return YES;
}
@end
