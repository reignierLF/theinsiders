//
//  LoginViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 15/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "ForgotPasswordViewController.h"

#import "TimerView.h"
#import "SwitchView.h"
#import "ActivationView.h"
#import "SignUpView.h"
#import "LoginView.h"

#import "Api.h"
#import "ActivationCode.h"
#import "SignUp.h"
#import "Login.h"
#import "UserCredentials.h"

#import "LoadingScreen.h"

#import <linkedin-sdk/LISDK.h>

@interface LoginViewController () <SignUpDelegate, LoginDelegate, ActivationDelegate, TimerDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) NSString *activationCode;

@property (nonatomic, strong) UILabel *headerLabel;
@property (nonatomic, strong) UIView *underline;

@property (nonatomic, strong) ActivationView *activationView;
@property (nonatomic, strong) SignUpView *signupView;
@property (nonatomic, strong) LoginView *loginView;
@property (nonatomic, strong) SwitchView *loginSwitchView;
@property (nonatomic, strong) SwitchView *signUpSwitchView;

@property (nonatomic, strong) LoadingScreen *ls;
@property (nonatomic, strong) TimerView *tv;

@property (nonatomic, strong) Api *api;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
/*
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Please enter the activation code"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.placeholder = @"Activation Code";
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    */
    // Do any additional setup after loading the view.
    
//    NSArray *objectsToShare = @[@"www.google.com"];
//    
//    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
//    
//    NSArray *excludedActivities = @[UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
//                                    UIActivityTypePostToWeibo,
//                                    UIActivityTypeMessage, UIActivityTypeMail,
//                                    UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
//                                    UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
//                                    UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
//                                    UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
//    controller.excludedActivityTypes = excludedActivities;
//    
//    [self presentViewController:controller animated:YES completion:nil];
    
    [self initialize];
    [self initBackgroundImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden = YES;
    self.title = @"";
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _api = [[Api alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
    //[self.view addSubview:_ls];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App BG"]];
    backgroundImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
    
    UIImageView *headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"The Insiders Hires Transp"]];
    headerImageView.frame = CGRectMake((_screenWidth / 8), 80, _screenWidth - ((_screenWidth / 8) * 2), 60);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    headerImageView.contentMode = UIViewContentModeScaleAspectFit;
    headerImageView.clipsToBounds = YES;
    [self.view addSubview:headerImageView];
    
    _headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(headerImageView.frame.origin.x, headerImageView.frame.origin.y + headerImageView.frame.size.height + 10, headerImageView.frame.size.width, 40)];
    //headerLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
    _headerLabel.text = @"Welcome to the most exclusive\nnightlife experience.";
    _headerLabel.textColor = [UIColor whiteColor];
    _headerLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    _headerLabel.textAlignment = NSTextAlignmentCenter;
    _headerLabel.numberOfLines = 0;
    [self.view addSubview:_headerLabel];
    
    [self initHeaderButton];
    //[self initLaunchingTimer];
}

-(void)initLaunchingTimer{

    _tv = [[TimerView alloc] initWithFrame:CGRectMake(0, _headerLabel.frame.origin.y + _headerLabel.frame.size.height, _screenWidth, _screenHeight - (_headerLabel.frame.origin.y + _headerLabel.frame.size.height + 10))];
    _tv.delegate = self;
    [self.view addSubview:_tv];
}

-(void)initHeaderButton{
    
    UIButton *signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //signUpButton.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    signUpButton.frame = CGRectMake(0, _headerLabel.frame.origin.y + _headerLabel.frame.size.height + 20, _screenWidth / 2, 32);
    signUpButton.tag = 0;
    signUpButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [signUpButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:signUpButton];
    
    [signUpButton addTarget:self action:@selector(switchEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //loginButton.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.2];
    loginButton.frame = CGRectMake(_screenWidth / 2, signUpButton.frame.origin.y, _screenWidth / 2, signUpButton.frame.size.height);
    loginButton.tag = 1;
    loginButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [loginButton setTitle:@"LOG IN" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:loginButton];
    
    [loginButton addTarget:self action:@selector(switchEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *underLineHolder = [[UIView alloc] initWithFrame:CGRectMake(0, (loginButton.frame.origin.y + loginButton.frame.size.height) - 1.5, _screenWidth, 1.5)];
    underLineHolder.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.8];
    [self.view addSubview:underLineHolder];
    
    _underline = [[UIView alloc] initWithFrame:CGRectMake(loginButton.frame.origin.x, (loginButton.frame.origin.y + loginButton.frame.size.height) - 3, loginButton.frame.size.width, 3)];
    _underline.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_underline];
    
    [self initSwitchingView];
}

-(void)initSwitchingView{
    
    float subViewHeight = _screenHeight - (_underline.frame.origin.y + _underline.frame.size.height);
    
    _activationView = [[ActivationView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, subViewHeight)];
    _activationView.delegate = self;
    
    _signupView = [[SignUpView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth,                                                                          subViewHeight)];
    //signupView.backgroundColor = [UIColor yellowColor];
    _signupView.delegate = self;
    
    _loginView = [[LoginView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, subViewHeight)];
    //loginView.backgroundColor = [UIColor orangeColor];
    _loginView.delegate = self;
    
    _signUpSwitchView = [[SwitchView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, subViewHeight)];
    _signUpSwitchView.firstView = _activationView;
    _signUpSwitchView.secondView = _signupView;
    
    _loginSwitchView = [[SwitchView alloc] initWithFrame:CGRectMake(0, _underline.frame.origin.y + _underline.frame.size.height, _screenWidth, subViewHeight)];
    //_loginSwitchView.backgroundColor = [UIColor brownColor];
    _loginSwitchView.firstView = _signUpSwitchView;
    _loginSwitchView.secondView = _loginView;
    [self.view addSubview:_loginSwitchView];
    
    [_signUpSwitchView switchViewWithIndex:0];
}

-(void)switchEvent:(UIButton*)sender{
    
    NSLog(@"switch view");
    
    [_loginSwitchView switchViewWithIndex:sender.tag];
    
    [UIView animateWithDuration: 0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         _underline.alpha = 0.5;

                         _underline.frame = CGRectMake(sender.frame.origin.x, _underline.frame.origin.y, _underline.frame.size.width, _underline.frame.size.height);
                         
                     }completion:^(BOOL finished){
                         if(finished){
                             
                             [_signUpSwitchView switchViewWithIndex:0];
                             
                             [UIView animateWithDuration: 0.3
                                                   delay: 0.0
                                                 options: UIViewAnimationOptionCurveEaseInOut
                                              animations: ^{
                                                  
                                                  _underline.alpha = 1.0;
                                                  
                                                  [_activationView dismissAll];
                                                  [_loginView dismissAll];
                                                  [_signupView dismissAll];
                                                  
                                              }completion:nil];
                         }
                     }];
}

-(void)activate:(ActivationView *)activationView code:(NSString *)code{
    
    _activationCode = code;
    
    [_ls loadingScreen:^{
       
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api activationCode:code isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"activation json = %@", jsonDictionary);
                
                ActivationCode *ac = [[ActivationCode alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(!ac.status){
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:ac.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        }else{
                            
                            [_signUpSwitchView switchViewWithIndex:1];
                            
                            NSLog(@"success activation code proceed to sign-up page");
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at activation code");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
    
    NSLog(@"activate code");
}

-(void)getStartedEvent:(SignUpView *)signUpView email:(NSString *)email password:(NSString *)password firstName:(NSString *)fName lastName:(NSString *)lName dob:(NSString *)dob mobileNo:(NSString *)mobileNo{
    
    [_ls loadingScreen:^{
       
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api signUpWithEmail:email password:password firstName:fName lastName:lName dob:dob mobileNo:mobileNo activationCode:_activationCode isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"sign-up json = %@", jsonDictionary);
                
                SignUp *su = [[SignUp alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(su.status){
                            
                            /*
                             * This code shows pop-up message when successful register
                             *
                             
                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:su.message preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                             
                             UserCredentials *uc = [[UserCredentials alloc] init];
                             [uc setAuthenticationToken:su.authenticationToken];
                             [uc setDob:su.dob];
                             [uc setEmail:su.email];
                             [uc setGender:su.gender];
                             [uc setUserId:su.loginId];
                             [uc setMobileNo:su.mobileNo];
                             [uc setName:su.name];
                             [uc setQrCodeInfoUrl:su.qrCodeInfoUrl];
                             [uc setSalutation:su.salutation];
                             [uc setSignInCount:su.signInCount];
                             
                             HomeViewController *home = [[HomeViewController alloc] init];
                             [self.navigationController pushViewController:home animated:YES];
                             }];
                             
                             [alertController addAction:close];
                             
                             [self presentViewController:alertController animated:YES completion:nil];
                             */
                            
                            UserCredentials *uc = [[UserCredentials alloc] init];
                            [uc setAuthenticationToken:su.authenticationToken];
                            [uc setDob:su.dob];
                            [uc setEmail:su.email];
                            [uc setGender:su.gender];
                            [uc setUserId:su.loginId];
                            [uc setMobileNo:su.mobileNo];
                            [uc setName:su.name];
                            [uc setQrCodeInfoUrl:su.qrCodeInfoUrl];
                            [uc setSalutation:su.salutation];
                            [uc setSignInCount:su.signInCount];
                            [uc setLoginAs:@"normal"];
                            
                            HomeViewController *home = [[HomeViewController alloc] init];
                            [self.navigationController pushViewController:home animated:YES];
                            
                            _underline.frame = CGRectMake(_underline.frame.origin.x, _underline.frame.origin.y, _underline.frame.size.width, _underline.frame.size.height);
                            
                            [_loginSwitchView switchViewWithIndex:1];
                            [_signUpSwitchView switchViewWithIndex:0];
                            
                            NSLog(@"success sign-up");
                        }else{
                        
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:su.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at sign-up");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
    
    NSLog(@"get started");
}

-(void)activateWithLinkedIn:(ActivationView*)activationView code:(NSString *)code{
    
    [_ls loadingScreen:^{
        
        [LISDKSessionManager
         createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION,LISDK_EMAILADDRESS_PERMISSION, nil]
         state:@"test state"
         showGoToAppStoreDialog:YES
         successBlock:^(NSString *returnState) {
             NSLog(@"%s","success called!");
             LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
             NSLog(@"value=%@ isvalid=%@",[session value],[session isValid] ? @"YES" : @"NO");
             NSMutableString *text = [[NSMutableString alloc] initWithString:[session.accessToken description]];
             [text appendString:[NSString stringWithFormat:@",state=\"%@\"",returnState]];
             NSLog(@"Response label text %@",text);
             NSLog(@"accessToken : %@",session.accessToken.description);
             
             NSString *url = [NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)"];
             
             if ([LISDKSessionManager hasValidSession]) {
                 [[LISDKAPIHelper sharedInstance] getRequest:url
                                                     success:^(LISDKAPIResponse *response) {
                                                         
                                                         NSLog(@"%@",response.data);
                                                         
                                                         NSLog(@"success");
                                                         
                                                         [self signUpLinkedInWithData:response.data code:code];
                                                     } error:^(LISDKAPIError *apiError) {
                                                         
                                                         [_ls finishLoading];
                                                         
                                                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:apiError.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                                                         
                                                         UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                                                         [alertController addAction:close];
                                                         
                                                         [self presentViewController:alertController animated:YES completion:nil];
                                                         
                                                         NSLog(@"failed LISDKAPIError");
                                                     }];
             }
             
             NSLog(@"login with linkedin then signup");
         }errorBlock:^(NSError* error) {
             
             [_ls finishLoading];
             
             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:error.description preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
             [alertController addAction:close];
             
             [self presentViewController:alertController animated:YES completion:nil];
             
             NSLog(@"%s","error called!");
         }];
    }];

    NSLog(@"activation code : %@", code);
    NSLog(@"sign up with linkedin");
}

-(NSArray*)trimmedLinkedInUserData:(NSString*)data{

    NSString *trimEmailAddress = [data stringByReplacingOccurrencesOfString:            @"{\n  \"emailAddress\": \""    withString:@""];
    NSString *trimFirstName = [trimEmailAddress stringByReplacingOccurrencesOfString:   @"\n  \"firstName\": \""        withString:@""];
    NSString *trimId = [trimFirstName stringByReplacingOccurrencesOfString:             @"\n  \"id\": \""               withString:@""];
    NSString *trimLastName = [trimId stringByReplacingOccurrencesOfString:              @"\n  \"lastName\": \""         withString:@""];
    NSString *finalTrimming = [trimLastName stringByReplacingOccurrencesOfString:       @"\"\n}"                        withString:@""];
    
    NSArray *array = [finalTrimming componentsSeparatedByString:@"\","];
    
    return array;
}

-(void)signUpLinkedInWithData:(NSString*)data code:(NSString*)code{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [_api linkedInSignUpWithEmail:[NSString stringWithFormat:@"%@",[[self trimmedLinkedInUserData:data] objectAtIndex:0]] firstName:[NSString stringWithFormat:@"%@",[[self trimmedLinkedInUserData:data] objectAtIndex:1]] linkedInUserId:[NSString stringWithFormat:@"%@",[[self trimmedLinkedInUserData:data] objectAtIndex:2]] lastName:[NSString stringWithFormat:@"%@",[[self trimmedLinkedInUserData:data] objectAtIndex:3]] activationCode:code isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
            
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
            
            NSLog(@"sign-up json = %@", jsonDictionary);
            
            SignUp *su = [[SignUp alloc] initWithDictionary:jsonDictionary];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isComplete){
                    
                    if(!su.status){
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:su.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }else{
                      
                        UserCredentials *uc = [[UserCredentials alloc] init];
                        [uc setAuthenticationToken:su.authenticationToken];
                        [uc setDob:su.dob];
                        [uc setEmail:su.email];
                        [uc setGender:su.gender];
                        [uc setUserId:su.loginId];
                        [uc setMobileNo:su.mobileNo];
                        [uc setName:su.name];
                        [uc setQrCodeInfoUrl:su.qrCodeInfoUrl];
                        [uc setSalutation:su.salutation];
                        [uc setSignInCount:su.signInCount];
                        [uc setLoginAs:@"linkedIn"];
                        
                        HomeViewController *home = [[HomeViewController alloc] init];
                        [self.navigationController pushViewController:home animated:YES];
                        
                        _underline.frame = CGRectMake(_underline.frame.origin.x, _underline.frame.origin.y, _underline.frame.size.width, _underline.frame.size.height);
                        
                        [_loginSwitchView switchViewWithIndex:1];
                        [_signUpSwitchView switchViewWithIndex:0];
                        
                        NSLog(@"success sign-up");
                    }
                }else{
                    
                    NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:close];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                    NSLog(@"failed to load json at sign-up");
                }
                
                [_ls finishLoading];
            });
        }];
    });
}

-(void)loginEvent:(LoginView *)loginView email:(NSString *)email password:(NSString *)password{
    
    [_ls loadingScreen:^{
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api loginWithEmail:email password:password isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"login json = %@", jsonDictionary);
                
                Login *log = [[Login alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(!log.status){
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:log.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        }else{
                            
                            UserCredentials *uc = [[UserCredentials alloc] init];
                            [uc setAuthenticationToken:log.authenticationToken];
                            [uc setDob:log.dob];
                            [uc setEmail:log.email];
                            [uc setGender:log.gender];
                            [uc setUserId:log.loginId];
                            [uc setMobileNo:log.mobileNo];
                            [uc setName:log.name];
                            [uc setQrCodeInfoUrl:log.qrCodeInfoUrl];
                            [uc setSalutation:log.salutation];
                            [uc setSignInCount:log.signInCount];
                            [uc setLoginAs:@"normal"];
                            
                            HomeViewController *home = [[HomeViewController alloc] init];
                            [self.navigationController pushViewController:home animated:YES];
                            
                            NSLog(@"success login");
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at login");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
    
    NSLog(@"login");
}

-(void)loginLinkedInEvent:(LoginView *)loginView{
    
    [_ls loadingScreen:^{
       
        [LISDKSessionManager
         createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION,LISDK_EMAILADDRESS_PERMISSION, nil]
         state:@"test state"
         showGoToAppStoreDialog:YES
         successBlock:^(NSString *returnState) {
             NSLog(@"%s","success called!");
             LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
             NSLog(@"value=%@ isvalid=%@",[session value],[session isValid] ? @"YES" : @"NO");
             NSMutableString *text = [[NSMutableString alloc] initWithString:[session.accessToken description]];
             [text appendString:[NSString stringWithFormat:@",state=\"%@\"",returnState]];
             NSLog(@"Response label text %@",text);
             NSLog(@"accessToken : %@",session.accessToken.description);
             
             NSString *url = [NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)"];
             
             if ([LISDKSessionManager hasValidSession]) {
                 [[LISDKAPIHelper sharedInstance] getRequest:url
                                                     success:^(LISDKAPIResponse *response) {
                                                         
                                                         NSLog(@"%@",response.data);
                                                         
                                                         NSLog(@"success");
                                                         
                                                         [self loginLinkedInWithData:response.data];
                                                     } error:^(LISDKAPIError *apiError) {
                                                         
                                                         [_ls finishLoading];
                                                         
                                                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:apiError.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                                                         
                                                         UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                                                         [alertController addAction:close];
                                                         
                                                         [self presentViewController:alertController animated:YES completion:nil];
                                                         
                                                         NSLog(@"failed LISDKAPIError");
                                                     }];
             }
             
             NSLog(@"login with linkedin");
         }errorBlock:^(NSError* error) {
             
             [_ls finishLoading];
             
             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:error.description preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
             [alertController addAction:close];
             
             [self presentViewController:alertController animated:YES completion:nil];
             
             NSLog(@"%s","error called!");
         }];
    }];
}

-(void)loginLinkedInWithData:(NSString*)data{

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [_api linkedInLoginWithEmail:[[self trimmedLinkedInUserData:data] objectAtIndex:0] linkedInUserId:[[self trimmedLinkedInUserData:data] objectAtIndex:2] isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
        
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
            
            NSLog(@"login json = %@", jsonDictionary);
            
            Login *log = [[Login alloc] initWithDictionary:jsonDictionary];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isComplete){
                    
                    if(!log.status){
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:log.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }else{
                        
                        UserCredentials *uc = [[UserCredentials alloc] init];
                        [uc setAuthenticationToken:log.authenticationToken];
                        [uc setDob:log.dob];
                        [uc setEmail:log.email];
                        [uc setGender:log.gender];
                        [uc setUserId:log.loginId];
                        [uc setMobileNo:log.mobileNo];
                        [uc setName:log.name];
                        [uc setQrCodeInfoUrl:log.qrCodeInfoUrl];
                        [uc setSalutation:log.salutation];
                        [uc setSignInCount:log.signInCount];
                        [uc setLoginAs:@"linkedIn"];
                        
                        HomeViewController *home = [[HomeViewController alloc] init];
                        [self.navigationController pushViewController:home animated:YES];
                        
                        NSLog(@"success login");
                    }
                }else{
                    
                    NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:close];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                    NSLog(@"failed to load json at login");
                }
                
                [_ls finishLoading];
            });
        }];
    });
}

-(void)forgotPasswordEvent:(LoginView *)loginView{

    ForgotPasswordViewController *forgotPassword = [[ForgotPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgotPassword animated:YES];
}

-(void)loginKeyboardWillShow:(NSNotification *)notification{
    
    /*
     * Get the height of the keyboard and subtract 30px
     * the 30px is the auto-complete words appear at the
     * top of keyboard
     */
    
    float keyBoardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height - 30;
    
    /*
     * Check if screen is on default position
     * Screen y-axis = 0
     */
    
    if(self.view.frame.origin.y >= 0){
        
        /*
         * Move the screen upward base on the height of the keyboard
         * Screen Y-axis subtract by height of keyboard
         * Animation is done automatically
         */
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - (keyBoardHeight / 2), self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)loginKeyboardWillBeHidden:(NSNotification *)notification{
    
    /*
     * Reposition of screen by moving downward
     * Back to default Y-axis position
     */
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}


-(void)signUpKeyboardWillShow:(NSNotification *)notification{
    
    /*
     * Get the height of the keyboard and subtract 30px
     * the 30px is the auto-complete words appear at the
     * top of keyboard
     */
    
    float keyBoardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    /*
     * Check if screen is on default position
     * Screen y-axis = 0
     */
    
    if(self.view.frame.origin.y >= 0){
        
        /*
         * Move the screen upward base on the height of the keyboard
         * Screen Y-axis subtract by height of keyboard
         * Animation is done automatically
         */
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - (keyBoardHeight / 2), self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)signUpKeyboardWillBeHidden:(NSNotification *)notification{
    
    /*
     * Reposition of screen by moving downward
     * Back to default Y-axis position
     */
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

-(void)didFinishCountingDown:(TimerView *)timerView{

    //[self initHeaderButton];
    
    [UIView animateWithDuration: 0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         _tv.alpha = 0.0;
                         
                     }completion:^(BOOL complete){
                     
                         
                     }];
}
@end
