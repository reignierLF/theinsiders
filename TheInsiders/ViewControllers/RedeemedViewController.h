//
//  RedeemedViewController.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 9/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedeemedViewController : UIViewController

@property (nonatomic, strong) NSString *iceBreakerTitle;
@property (nonatomic, strong) NSString *iceBreakerDesciption;
@property (nonatomic, strong) NSString *iceBreakerImageUrl;
@property (nonatomic) BOOL isRedeemed;

@end
