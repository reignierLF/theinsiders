//
//  HomeViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "HomeViewController.h"
#import "EventViewController.h"
#import "Carousel.h"

#import "MenuController.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "UpdateSalutation.h"
#import "AllEvents.h"

@interface HomeViewController () <MenuControllerDelegate, CarouselDelegate, UIScrollViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;

@property (nonatomic, strong) MenuController *mc;
@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;
@property (nonatomic, strong) AllEvents *ae;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self initialize];
    [self initBackgroundImage];
    
    if([_uc.signInCount integerValue] <= 1){
    
        [self salutationActionSheet];
    }else{
        
        [self initAllEvents];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self initMenuController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Layout

-(void)initialize{
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - _navigationBarHeight;
    
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Home";
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Futura ICG" size:18] };
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
    //[self.view addSubview:_ls];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App BG"]];
    backgroundImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initAllEvents{
    
    [_ls loadingScreen:^{
       
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            //NSLog(@"token = %@",_uc.authenticationToken);
            
            [_api getAllEventsWithToken:_uc.authenticationToken isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"all events json = %@", jsonDictionary);
                
                _ae = [[AllEvents alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(!_ae.status){
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:_ae.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        }else{
                            
                            if(_ae.size == 0){
                                
                                [self initNoEvents];
                            }else{
                                
                                [self initCarousel];
                            }
                            
                            NSLog(@"success all events");
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at all events");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)initCarousel{

    Carousel *carousel = [[Carousel alloc] initWithFrame:CGRectMake(0, _navigationBarHeight, _screenWidth, _screenHeight)];
    carousel.delegate = self;
    carousel.size = _ae.size;
    carousel.allAddress = _ae.address;
    carousel.allEventImageUrl = _ae.eventImageUrl;
    carousel.allEventId = _ae.eventId;
    carousel.allDateTime = _ae.dateTime;
    [self.view addSubview:carousel];
}

-(void)initNoEvents{
    
    UIImageView *headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"The Insiders Hires Transp"]];
    headerImageView.frame = CGRectMake((_screenWidth / 8), (_screenHeight / 2) - 60, _screenWidth - ((_screenWidth / 8) * 2), 60);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    headerImageView.contentMode = UIViewContentModeScaleAspectFit;
    headerImageView.clipsToBounds = YES;
    [self.view addSubview:headerImageView];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(headerImageView.frame.origin.x, headerImageView.frame.origin.y + headerImageView.frame.size.height, headerImageView.frame.size.width, 120)];
    //headerLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
    headerLabel.text = @"Looks like there are no\nupcoming events.\n\nCheck back soon!";
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"Futura ICG" size:20];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.numberOfLines = 0;
    [self.view addSubview:headerLabel];
}

-(void)initMenuController{
    
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-(_screenWidth - (_screenWidth / 4)), 0, _screenWidth - (_screenWidth / 2.5), _screenHeight)];
    _mc.delegate = self;
    _mc.menu.settings.menuBackgroundImage = @"Insider App BG";
    _mc.menu.settings.textColor = [UIColor whiteColor];
    _mc.menu.settings.font = [UIFont fontWithName:@"Futura ICG" size:16];
    _mc.menu.settings.linebreakColor = [UIColor clearColor];
    _mc.popToFront = YES;
    _mc.headerSpace = 50;
    _mc.textMargin = 30;
    [self.view addSubview:_mc];
    
    UILabel *userFullNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 60, _mc.frame.size.width - 40, 50)];
    //userFullNameLabel.backgroundColor = [UIColor redColor];
    userFullNameLabel.text = [NSString stringWithFormat:@"%@",_uc.name];
    userFullNameLabel.textAlignment = NSTextAlignmentCenter;
    userFullNameLabel.textColor = [UIColor whiteColor];
    userFullNameLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [_mc addSubview:userFullNameLabel];
}

#pragma mark Events

-(void)salutationActionSheet{
    
    /*
     * Instantiate view for action sheet
     */
    
    UIAlertController *actionView = [UIAlertController
                                     alertControllerWithTitle:@"Choose Your Salutation"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleActionSheet];
    
    /*
     * List of option of action sheet
     *
     * Help / FAQ
     * Logout
     * Cancel
     */
    
    UIAlertAction* ac1 = [UIAlertAction
                          actionWithTitle:@"Mr."
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              
                              [self submitWithSalutation:@"Mr" completion:^{
                                  
                                  [self initAllEvents];
                              }];
                              
                              [actionView dismissViewControllerAnimated:YES completion:nil];
                          }];
    
    UIAlertAction* ac2 = [UIAlertAction
                          actionWithTitle:@"Ms."
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              
                              [self submitWithSalutation:@"Ms" completion:^{
                                  
                                  [self initAllEvents];
                              }];
                              
                              [actionView dismissViewControllerAnimated:YES completion:nil];
                          }];
    
    UIAlertAction* ac3 = [UIAlertAction
                          actionWithTitle:@"Mrs."
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              
                              [self submitWithSalutation:@"Mrs" completion:^{
                                  
                                  [self initAllEvents];
                              }];
                              
                              [actionView dismissViewControllerAnimated:YES completion:nil];
                          }];
    
    /*
     * Added them all in action controller
     */
    
    [actionView addAction:ac1];
    [actionView addAction:ac2];
    [actionView addAction:ac3];
    [self presentViewController:actionView animated:YES completion:nil];
}

-(void)submitWithSalutation:(NSString*)salutation completion:(void (^)(void))callbackBlock {
    
    NSLog(@"submit salutation");
    
    [_ls loadingScreen:^{
       
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api updateSalutationWithToken:_uc.authenticationToken salutation:salutation loginId:_uc.userId isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"update salutation json = %@", jsonDictionary);
                
                UpdateSalutation *us = [[UpdateSalutation alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        /*
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:us.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:^{
                            
                            callbackBlock();
                            
                            NSLog(@"success update salutation");
                        }];
                        */
                        callbackBlock();
                        
                        NSLog(@"success update salutation");
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at update salutation");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

#pragma mark Delegates

-(void)didChooseEventCard:(Carousel *)carousel sender:(UIButton *)sender{
    
    NSLog(@"eventId = %ld",(long)sender.tag);

    EventViewController *evc = [[EventViewController alloc] init];
    evc.index = sender.tag;
    evc.eventId = [carousel.allEventId objectAtIndex:sender.tag];
    [self.navigationController pushViewController:evc animated:YES];
}

-(void)didRefreshCurrentViewController{
    
}

-(void)didSelectMenu:(MenuController *)menuController viewController:(UIViewController *)viewController{
    
    NSLog(@"viewController = %@",viewController);
    
    if([viewController.title isEqualToString:@"Sign Out"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sign out" message:@"Are you sure you want to sign out?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* signout = [UIAlertAction actionWithTitle:@"Sign out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.view.clipsToBounds = YES;
            
            [self garbageCollector:nil];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        
        [alertController addAction:signout];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self.navigationController pushViewController:viewController animated:NO];
            
            [UIView transitionFromView:self.view
                                toView:viewController.view
                              duration:0.25
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            completion:^(BOOL isComplete){
                                
                                [self garbageCollector:viewController];
                            }];
        });
    }
}

-(void)didSelectSubMenu:(MenuController *)menuController{
    
    NSLog(@"Did select sub menu");
}

-(void)garbageCollector:(UIViewController*)vc{

    _mc.menu.viewControllers = nil;
    [_mc.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _mc = nil;
    
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if(vc != nil){
        
        NSMutableArray *viewControllerArray = [self.navigationController.viewControllers mutableCopy];
        
        UIViewController *uv = [viewControllerArray objectAtIndex:1];
        
        [uv.view removeFromSuperview];
        uv = nil;
        
        [viewControllerArray removeObjectAtIndex:1];
        
        vc.navigationController.viewControllers = viewControllerArray;
    }
}
@end
