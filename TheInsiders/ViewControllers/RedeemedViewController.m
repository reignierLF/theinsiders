//
//  RedeemedViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 9/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "RedeemedViewController.h"

#import "ImageLoader.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "Redeem.h"

@interface RedeemedViewController ()<UIScrollViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIScrollView *slideToRedeem;
@property (nonatomic, strong) UIImageView *redeemedImageView;

@property (nonatomic, strong) ImageLoader *il;
@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;

@end

@implementation RedeemedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"title : %@",_iceBreakerTitle);
    NSLog(@"des : %@",_iceBreakerDesciption);
    NSLog(@"image : %@",_iceBreakerImageUrl);
    NSLog(@"isredeemed %i",_isRedeemed);
    
    [self initialize];
    [self initBackgroundImage];
    [self initLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - _navigationBarHeight;
    
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Icebreaker";
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Futura ICG" size:18] };
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
    _il = [[ImageLoader alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
    //[self.view addSubview:_ls];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App BG"]];
    backgroundImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initLayout{

    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _navigationBarHeight + 10, _screenWidth - 40, 50)];
    //headerLabel.backgroundColor = [UIColor redColor];
    //headerLabel.text = @"Make your way around the room and look for\nthe person whose image matches with yours.\n\nPresent both your phones at the bar and\nredeem your free drink!";
    headerLabel.text = _iceBreakerDesciption;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    headerLabel.numberOfLines = 0;
    headerLabel.lineBreakMode = NSLineBreakByWordWrapping;
    headerLabel.frame = CGRectMake(headerLabel.frame.origin.x, headerLabel.frame.origin.y, headerLabel.frame.size.width, headerLabel.intrinsicContentSize.height);
    [self.view addSubview:headerLabel];
    
    float imageViewHeight = _screenHeight - ((_screenHeight / 6) * 2);
    
    UIImageView *drinkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headerLabel.frame.origin.x + 20, headerLabel.frame.origin.y + headerLabel.frame.size.height + 10, headerLabel.frame.size.width - 40, imageViewHeight)];
    //drinkImageView.image = [UIImage imageNamed:@"alcoholDrink"];
    drinkImageView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    drinkImageView.contentMode = UIViewContentModeScaleAspectFill;
    drinkImageView.clipsToBounds = YES;
    [self.view addSubview:drinkImageView];
    
    [_il parseImage:drinkImageView url:_iceBreakerImageUrl errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhite];
    
    float redeemedWidth = drinkImageView.frame.size.width - 40;
    float redeemedHeight = 100;
    
    _redeemedImageView = [[UIImageView alloc] initWithFrame:CGRectMake((_screenWidth / 2) - (redeemedWidth / 2), ((_screenHeight / 2) + _navigationBarHeight) - (redeemedHeight / 2), redeemedWidth, redeemedHeight)];
    _redeemedImageView.image = [UIImage imageNamed:@"Redeemed"];
    //redeemedImageView.backgroundColor = [UIColor orangeColor];
    _redeemedImageView.contentMode = UIViewContentModeScaleAspectFit;
    _redeemedImageView.clipsToBounds = YES;
    _redeemedImageView.alpha = 0.0;
    [self.view addSubview:_redeemedImageView];
    
    float sliderheight = (_screenHeight + _navigationBarHeight) - (drinkImageView.frame.origin.y + drinkImageView.frame.size.height + 10);
    
    if(sliderheight >= 60){
    
        sliderheight = 60;
    }
    
    _slideToRedeem = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (_screenHeight + _navigationBarHeight) - sliderheight, _screenWidth, sliderheight)];
    _slideToRedeem.delegate = self;
    _slideToRedeem.contentSize = CGSizeMake(_slideToRedeem.frame.size.width * 2, _slideToRedeem.frame.size.height);
    _slideToRedeem.showsHorizontalScrollIndicator = NO;
    _slideToRedeem.bounces = NO;
    _slideToRedeem.pagingEnabled = YES;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _slideToRedeem.bounds;
    gradient.frame = CGRectMake(gradient.frame.origin.x, gradient.frame.origin.y, gradient.frame.size.width * 2, gradient.frame.size.height);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor cyanColor] CGColor], (id)[[UIColor purpleColor] CGColor], nil];
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    [_slideToRedeem.layer insertSublayer:gradient atIndex:0];

    [self.view addSubview:_slideToRedeem];
    
    [_slideToRedeem scrollRectToVisible:CGRectMake(_screenWidth, 0, _slideToRedeem.frame.size.width, _slideToRedeem.frame.size.height) animated:YES];
    
    UILabel *slideToRedeemLabel = [[UILabel alloc] initWithFrame:CGRectMake(_slideToRedeem.frame.size.width, 0, _screenWidth, _slideToRedeem.frame.size.height)];
    //headerLabel.backgroundColor = [UIColor redColor];
    
    if(_isRedeemed){
    
        _slideToRedeem.userInteractionEnabled = NO;
        slideToRedeemLabel.text = @"";
    }else{
    
        slideToRedeemLabel.text = @"> Slide To Redeem";
    }

    slideToRedeemLabel.textAlignment = NSTextAlignmentCenter;
    slideToRedeemLabel.textColor = [UIColor whiteColor];
    slideToRedeemLabel.font = [UIFont fontWithName:@"Futura ICG" size:28];
    [_slideToRedeem addSubview:slideToRedeemLabel];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    if(scrollView.contentOffset.x < scrollView.frame.size.width){
        
        [_ls loadingScreen:^{
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                [_api getRedeemWithToken:_uc.authenticationToken isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                    
                    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                    
                    NSLog(@"redeemed json = %@", jsonDictionary);
                    
                    Redeem *rd = [[Redeem alloc] initWithDictionary:jsonDictionary];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        
                        if(isComplete){
                            
                            if(rd.status){
                            
                                [UIView animateWithDuration: 0.4
                                                      delay: 0.0
                                                    options: UIViewAnimationOptionCurveEaseInOut
                                                 animations: ^{
                                                     
                                                     _redeemedImageView.alpha = 1.0;
                                                     
                                                 }completion:nil];
                                
                                scrollView.userInteractionEnabled = NO;
                                
                                
                                NSLog(@"Redeemed");
                            }else{
                                
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:rd.message preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                                [alertController addAction:close];
                                
                                [self presentViewController:alertController animated:YES completion:nil];
                            }
                            
                            NSLog(@"success redeemed");
                            
                        }else{
                            
                            NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        
                            NSLog(@"failed to load json at redeemed");
                        }
                        
                        [_ls finishLoading];
                    });
                }];
            });
        }];
    }
}
@end
