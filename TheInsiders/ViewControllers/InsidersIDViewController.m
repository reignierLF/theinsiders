//
//  InsidersIDViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 9/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "InsidersIDViewController.h"
#import "ChangePasswordViewController.h"

#import "MenuController.h"
#import "ImageLoader.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "Profile.h"

@interface InsidersIDViewController () <MenuControllerDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;

@property (nonatomic, strong) MenuController *mc;
@property (nonatomic, strong) ImageLoader *il;
@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;
@property (nonatomic, strong) Profile *pro;

@end

@implementation InsidersIDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    
    [_ls loadingScreen:^{
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api getProfileWithToken:_uc.authenticationToken isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"profile json = %@", jsonDictionary);
                
                _pro = [[Profile alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(!_pro.status){
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:_pro.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        }else{
                            
                            [self initLayout];
                            
                            NSLog(@"success profile");
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at profile");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)viewWillAppear:(BOOL)animated{

    [self initMenuController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - _navigationBarHeight;
    
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Insider Info";
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Futura ICG" size:18] };
    
    _il = [[ImageLoader alloc] init];
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
    //[self.view addSubview:_ls];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App BG"]];
    backgroundImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initLayout{

    UIView *gradientView = [[UIView alloc] initWithFrame:CGRectMake(0, _navigationBarHeight, _screenWidth, _screenHeight / 3)];
    gradientView.alpha = 0.8;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = gradientView.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor cyanColor] CGColor], (id)[[UIColor purpleColor] CGColor], nil];
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    [gradientView.layer insertSublayer:gradient atIndex:0];
    [self.view addSubview:gradientView];
    
    UILabel *userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _navigationBarHeight + 40, _screenWidth - 40, 30)];
    //userNameLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    userNameLabel.text = [NSString stringWithFormat:@"%@",_pro.name];
    userNameLabel.textAlignment = NSTextAlignmentCenter;
    userNameLabel.textColor = [UIColor whiteColor];
    userNameLabel.font = [UIFont fontWithName:@"Futura ICG" size:26];
    userNameLabel.numberOfLines = 0;
    userNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.view addSubview:userNameLabel];
    
    UILabel *userEmailLabel = [[UILabel alloc] initWithFrame:CGRectMake(userNameLabel.frame.origin.x , userNameLabel.frame.origin.y + userNameLabel.frame.size.height, userNameLabel.frame.size.width, 35)];
    //userEmailLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    userEmailLabel.text = [NSString stringWithFormat:@"%@", _pro.email];
    userEmailLabel.textColor = [UIColor whiteColor];
    userEmailLabel.textAlignment = NSTextAlignmentCenter;
    userEmailLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [self.view addSubview:userEmailLabel];
    
    float qrCodeSize = (_screenHeight / 3) - 20;
    
    UIImageView *qrCodeImageView = [[UIImageView alloc] initWithFrame:CGRectMake((_screenWidth / 2) - (qrCodeSize / 2), userEmailLabel.frame.origin.y + userEmailLabel.frame.size.height + 20, qrCodeSize, qrCodeSize)];
    //qrCodeImageView.backgroundColor = [UIColor brownColor];
    //qrCodeImageView.image = [UIImage imageNamed:@"qrcode"];
    qrCodeImageView.clipsToBounds = YES;
    qrCodeImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:qrCodeImageView];
    
    [_il parseImage:qrCodeImageView url:_pro.qrCodeUrl errorImageName:@"qrcode" style:UIActivityIndicatorViewStyleWhite];
    
    UILabel *contactLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, qrCodeImageView.frame.origin.y + qrCodeImageView.frame.size.height + 40, _screenWidth / 4, 35)];
    //contactLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    contactLabel.text = @"Contact";
    contactLabel.textColor = [UIColor whiteColor];
    contactLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    //contactLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:contactLabel];
    
    UILabel *birthdayLabel = [[UILabel alloc] initWithFrame:CGRectMake(contactLabel.frame.origin.x, contactLabel.frame.origin.y + contactLabel.frame.size.height, contactLabel.frame.size.width, contactLabel.frame.size.height)];
    //birthdayLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    birthdayLabel.text = @"Birthday";
    birthdayLabel.textColor = [UIColor whiteColor];
    birthdayLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    //birthdayLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:birthdayLabel];
    
    UILabel *genderLabel = [[UILabel alloc] initWithFrame:CGRectMake(birthdayLabel.frame.origin.x, birthdayLabel.frame.origin.y + birthdayLabel.frame.size.height, birthdayLabel.frame.size.width, birthdayLabel.frame.size.height)];
    //genderLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    genderLabel.text = @"Gender";
    genderLabel.textColor = [UIColor whiteColor];
    genderLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    //genderLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:genderLabel];
    
    NSLog(@"loginas %@",_uc.loginAs);
    
    if([_uc.loginAs isEqualToString:@"normal"]){
        
        UIButton *changePasswordButton = [UIButton buttonWithType:UIButtonTypeSystem];
        changePasswordButton.frame = CGRectMake(20, (_screenHeight + _navigationBarHeight) - (40 + 20), _screenWidth - 40, 40);
        [changePasswordButton setTitle:@"Change Password" forState:UIControlStateNormal];
        [changePasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        changePasswordButton.layer.borderColor = [UIColor lightTextColor].CGColor;
        changePasswordButton.layer.borderWidth = 2;
        changePasswordButton.layer.cornerRadius = 5;
        [self.view addSubview:changePasswordButton];
        
        [changePasswordButton addTarget:self action:@selector(changePasswordEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    //float userLabelWidth = _screenWidth - ((emailLabel.frame.origin.x * 2) + emailLabel.frame.size.width);
    
    //UILabel *userEmailLabel = [[UILabel alloc] initWithFrame:CGRectMake(emailLabel.frame.origin.x + emailLabel.frame.size.width, emailLabel.frame.origin.y, userLabelWidth, emailLabel.frame.size.height)];
    //userEmailLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //userEmailLabel.text = [NSString stringWithFormat:@"%@", _pro.email];
    //userEmailLabel.textColor = [UIColor grayColor];
    //userEmailLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    //[self.view addSubview:userEmailLabel];
    
    UILabel *userContactLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 2, contactLabel.frame.origin.y, (_screenWidth / 2) - 20, contactLabel.frame.size.height)];
    //userContactLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    userContactLabel.text = [NSString stringWithFormat:@"%@", _pro.mobileNo];
    userContactLabel.textColor = [UIColor grayColor];
    userContactLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [self.view addSubview:userContactLabel];
    
    UILabel *userBirthdayLabel = [[UILabel alloc] initWithFrame:CGRectMake(userContactLabel.frame.origin.x, birthdayLabel.frame.origin.y, userContactLabel.frame.size.width, birthdayLabel.frame.size.height)];
    //userContactLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    userBirthdayLabel.text = [NSString stringWithFormat:@"%@", _pro.dob];
    userBirthdayLabel.textColor = [UIColor grayColor];
    userBirthdayLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [self.view addSubview:userBirthdayLabel];
    
    UILabel *userGenderLabel = [[UILabel alloc] initWithFrame:CGRectMake(userBirthdayLabel.frame.origin.x, genderLabel.frame.origin.y, userBirthdayLabel.frame.size.width, genderLabel.frame.size.height)];
    //userContactLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    userGenderLabel.text = [NSString stringWithFormat:@"%@", _pro.gender];
    userGenderLabel.textColor = [UIColor grayColor];
    userGenderLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [self.view addSubview:userGenderLabel];
}

-(void)initMenuController{
    
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-(_screenWidth - (_screenWidth / 4)), 0, _screenWidth - (_screenWidth / 2.5), _screenHeight)];
    _mc.delegate = self;
    _mc.menu.settings.menuBackgroundImage = @"Insider App BG";
    _mc.menu.settings.textColor = [UIColor whiteColor];
    _mc.menu.settings.font = [UIFont fontWithName:@"Futura ICG" size:16];
    _mc.menu.settings.linebreakColor = [UIColor clearColor];
    _mc.popToFront = YES;
    _mc.headerSpace = 50;
    _mc.textMargin = 30;
    [self.view addSubview:_mc];
    
    UILabel *userFullNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 60, _mc.frame.size.width - 40, 50)];
    //userFullNameLabel.backgroundColor = [UIColor redColor];
    userFullNameLabel.text = [NSString stringWithFormat:@"%@",_uc.name];
    userFullNameLabel.textAlignment = NSTextAlignmentCenter;
    userFullNameLabel.textColor = [UIColor whiteColor];
    userFullNameLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [_mc addSubview:userFullNameLabel];
}

-(void)changePasswordEvent{
    
    ChangePasswordViewController *cp = [[ChangePasswordViewController alloc] init];
    [self.navigationController pushViewController:cp animated:YES];
    
    NSLog(@"change password");
}

-(void)didRefreshCurrentViewController{
    
}

-(void)didSelectMenu:(MenuController *)menuController viewController:(UIViewController *)viewController{
    
    NSLog(@"viewController = %@",viewController);
    
    if([viewController.title isEqualToString:@"Sign Out"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sign out" message:@"Are you sure you want to sign out?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* signout = [UIAlertAction actionWithTitle:@"Sign out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.view.clipsToBounds = YES;
            
            [self garbageCollector:nil];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        
        [alertController addAction:signout];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self.navigationController pushViewController:viewController animated:NO];
            
            [UIView transitionFromView:self.view
                                toView:viewController.view
                              duration:0.25
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            completion:^(BOOL isComplete){
                                
                                [self garbageCollector:viewController];
                            }];
        });
    }
}

-(void)didSelectSubMenu:(MenuController *)menuController{
    
    NSLog(@"Did select sub menu");
}

-(void)garbageCollector:(UIViewController*)vc{
    
    _mc.menu.viewControllers = nil;
    [_mc.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _mc = nil;
    
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if(vc != nil){
        
        NSMutableArray *viewControllerArray = [self.navigationController.viewControllers mutableCopy];
        
        UIViewController *uv = [viewControllerArray objectAtIndex:1];
        
        [uv.view removeFromSuperview];
        uv = nil;
        
        [viewControllerArray removeObjectAtIndex:1];
        
        vc.navigationController.viewControllers = viewControllerArray;
    }
}
@end
