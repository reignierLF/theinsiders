//
//  RsvpViewController.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 6/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RsvpViewController : UIViewController

@property (nonatomic, strong) NSString *eventId;

@end
