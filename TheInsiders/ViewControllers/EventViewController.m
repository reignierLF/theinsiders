//
//  EventViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 28/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "EventViewController.h"
#import "AdditionalInvite.h"
#import "RsvpViewController.h"

#import "cUIScrollView.h"
#import "PickerView.h"
#import "ParallaxEffect.h"
#import "ImageLoader.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "Event.h"
#import "RSVP.h"
#import "InviteGuests.h"
#import "ViewInvitations.h"

@interface EventViewController () <AdditionalInviteDelegate, PickerViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;

@property (nonatomic, strong) cUIScrollView *scrollView;

@property (nonatomic, strong) UIView *eventDetailBackgroundView;
@property (nonatomic, strong) UILabel *invitedCounterLabel;

@property (nonatomic, strong) NSMutableArray *invitedArray;

@property (nonatomic, strong) UIButton *rsvpButton;

@property (nonatomic, strong) AdditionalInvite *forSalutation;

@property (nonatomic, strong) ParallaxEffect *pe;
@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;
@property (nonatomic, strong) Event *event;

@end

@implementation EventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
}

-(void)viewWillAppear:(BOOL)animated{

    [_ls loadingScreen:^{
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api getEventWithToken:_uc.authenticationToken eventId:_eventId isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"event json = %@", jsonDictionary);
                
                _event = [[Event alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        [self initEventScrollView];
                        [self initEventLayout];
                        
                        NSLog(@"success event");
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at event");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)viewDidDisappear:(BOOL)animated{

    [_invitedArray removeAllObjects];
    
    [_scrollView removeFromSuperview];
    _scrollView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{

    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    self.view.backgroundColor = [UIColor colorWithRed:27.0f/255.0f green:27.0f/255.0f blue:27.0f/255.0f alpha:1.0];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Futura ICG" size:18] };
    
    //_isViewingOnly = NO;
    //_isDisableInviteAndRSVP = NO;
    
    _invitedArray = [[NSMutableArray alloc] init];
    _pe = [[ParallaxEffect alloc] init];
    _ls = [[LoadingScreen alloc] init];
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
    //_event = [[Event alloc] init];
}

-(void)initEventScrollView{

    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight + _navigationBarHeight)];
    _scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _scrollView.alpha = 0.0;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _screenHeight * 2);
    _scrollView.delaysContentTouches = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_scrollView];
    
    [UIView animateWithDuration: 0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         _scrollView.alpha = 1.0;
                     }completion:nil];
}

-(void)initEventLayout{

    ImageLoader *il = [[ImageLoader alloc] init];

    //UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"party"]];
    UIImageView *eventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-5, -5, _screenWidth + 10, 10 + (_screenHeight / 3))];
    eventImageView.backgroundColor = self.view.backgroundColor;
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    eventImageView.clipsToBounds = YES;
    [_scrollView addSubview:eventImageView];
    
    //il.isDisplayErrorImage = NO;
    [il parseImage:eventImageView url:_event.eventImageUrl errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhite];
    
    [_pe parallax:eventImageView intensity:20];
    
    UILabel *eventTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, _screenWidth - 80, _navigationBarHeight - 20)];
    //eventTitleLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //eventTitleLabel.text = @"Event Title";
    eventTitleLabel.text = [NSString stringWithFormat:@"%@",_event.title];
    eventTitleLabel.textColor = [UIColor whiteColor];
    eventTitleLabel.textAlignment = NSTextAlignmentCenter;
    eventTitleLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [_scrollView addSubview:eventTitleLabel];
    
    _eventDetailBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(30, (eventImageView.frame.origin.y + eventImageView.frame.size.height) - 80, _scrollView.frame.size.width - 60, 600)];
    _eventDetailBackgroundView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_eventDetailBackgroundView];
    
    UIImageView *dateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendarSmall"]];
    dateImageView.frame = CGRectMake(20, 30, 20, 20);
    //dateImageView.backgroundColor = [UIColor orangeColor];
    dateImageView.contentMode = UIViewContentModeScaleAspectFit;
    dateImageView.clipsToBounds = YES;
    [_eventDetailBackgroundView addSubview:dateImageView];
    
    float bulletPointSize = 15;
    float gapBetweenBulletPoint = 70;
    
    UIView *gradientBulletPoint = [[UIView alloc] initWithFrame:CGRectMake(0, 0, bulletPointSize * 6.5, bulletPointSize * 6.5)];
    gradientBulletPoint.center = CGPointMake((bulletPointSize * 6.5) / 2, (bulletPointSize * 6.5) / 2);
    gradientBulletPoint.backgroundColor = [UIColor whiteColor];
    gradientBulletPoint.layer.cornerRadius = gradientBulletPoint.frame.size.width / 2;
    gradientBulletPoint.alpha = 0.5;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = gradientBulletPoint.bounds;
    gradient.cornerRadius = gradientBulletPoint.layer.cornerRadius;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor cyanColor] CGColor], (id)[[UIColor purpleColor] CGColor], nil];
    [gradientBulletPoint.layer insertSublayer:gradient atIndex:0];
    [_eventDetailBackgroundView addSubview:gradientBulletPoint];
    
    UIView *whiteBulletPoint = [[UIView alloc] initWithFrame:CGRectMake(0, 0, bulletPointSize * 6, bulletPointSize * 6)];
    whiteBulletPoint.center = CGPointMake((bulletPointSize * 6) / 2, (bulletPointSize * 6) / 2);
    whiteBulletPoint.backgroundColor = [UIColor whiteColor];
    whiteBulletPoint.layer.cornerRadius = whiteBulletPoint.frame.size.width / 2;
    [_eventDetailBackgroundView addSubview:whiteBulletPoint];
    
    UIView *connectedBulletPointLine = [[UIView alloc] initWithFrame:CGRectMake(dateImageView.frame.origin.x + dateImageView.frame.size.width + 10 + ((bulletPointSize / 2) - 3), (dateImageView.frame.origin.y + dateImageView.frame.size.height) - bulletPointSize, 6, 100)];
    connectedBulletPointLine.backgroundColor = [UIColor darkGrayColor];
    connectedBulletPointLine.layer.borderColor = [UIColor whiteColor].CGColor;
    connectedBulletPointLine.layer.borderWidth = 2.5;
    [_eventDetailBackgroundView addSubview:connectedBulletPointLine];
    
    UIView *dateBulletPoint = [[UIView alloc] initWithFrame:CGRectMake(dateImageView.frame.origin.x + dateImageView.frame.size.width + 10, (dateImageView.frame.origin.y + (dateImageView.frame.size.height / 2)) - (bulletPointSize / 2), bulletPointSize, bulletPointSize)];
    dateBulletPoint.backgroundColor = [UIColor darkGrayColor];
    dateBulletPoint.layer.cornerRadius = dateBulletPoint.frame.size.width / 2;
    [_eventDetailBackgroundView addSubview:dateBulletPoint];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(dateBulletPoint.frame.origin.x + dateBulletPoint.frame.size.width + 10, dateImageView.frame.origin.y,(_eventDetailBackgroundView.frame.size.width - 20) - (dateBulletPoint.frame.origin.x + dateBulletPoint.frame.size.width + 10), dateImageView.frame.size.height)];
    //dateLabel.backgroundColor = [UIColor redColor];
    //dateLabel.text = @"20 January 2017, 8:00 PM";
    dateLabel.text = [NSString stringWithFormat:@"%@",_event.dateTime];
    dateLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    [_eventDetailBackgroundView addSubview:dateLabel];
    
    UIImageView *locationImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"placeholder"]];
    locationImageView.frame = CGRectMake(dateImageView.frame.origin.x, dateImageView.frame.origin.y + dateImageView.frame.size.height + gapBetweenBulletPoint, dateImageView.frame.size.width, dateImageView.frame.size.height);
    //dateImageView.backgroundColor = [UIColor orangeColor];
    locationImageView.contentMode = UIViewContentModeScaleAspectFit;
    locationImageView.clipsToBounds = YES;
    [_eventDetailBackgroundView addSubview:locationImageView];
    
    UIView *locationBulletPoint = [[UIView alloc] initWithFrame:CGRectMake(locationImageView.frame.origin.x + locationImageView.frame.size.width + 10, (locationImageView.frame.origin.y + (locationImageView.frame.size.height / 2)) - (bulletPointSize / 2), bulletPointSize, bulletPointSize)];
    locationBulletPoint.backgroundColor = [UIColor darkGrayColor];
    locationBulletPoint.layer.cornerRadius = locationBulletPoint.frame.size.width / 2;
    [_eventDetailBackgroundView addSubview:locationBulletPoint];
    
    float locationLabelOriginalHeight = 0;
    
    UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(locationBulletPoint.frame.origin.x + locationBulletPoint.frame.size.width + 10, locationImageView.frame.origin.y,(_eventDetailBackgroundView.frame.size.width - 20) - (locationBulletPoint.frame.origin.x + locationBulletPoint.frame.size.width + 10), locationImageView.frame.size.height)];
    
    locationLabelOriginalHeight = locationLabel.frame.size.height;
    
    //locationLabel.backgroundColor = [UIColor redColor];
    locationLabel.numberOfLines = 0;
    //locationLabel.text = @"Club Answer, Seoul, SK";
    locationLabel.text = [NSString stringWithFormat:@"%@",_event.address];
    locationLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    [locationLabel sizeToFit];
    [_eventDetailBackgroundView addSubview:locationLabel];
    
    if(locationLabel.frame.size.height < locationLabelOriginalHeight){
        
        locationLabel.frame = CGRectMake(locationLabel.frame.origin.x, locationLabel.frame.origin.y, locationLabel.frame.size.width, locationLabelOriginalHeight);
    }
    
    UIImageView *detailImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"balloon"]];
    detailImageView.frame = CGRectMake(locationImageView.frame.origin.x, locationLabel.frame.origin.y + locationLabel.frame.size.height + gapBetweenBulletPoint, locationImageView.frame.size.width, locationImageView.frame.size.height);
    //detailImageView.backgroundColor = [UIColor orangeColor];
    detailImageView.contentMode = UIViewContentModeScaleAspectFit;
    detailImageView.clipsToBounds = YES;
    [_eventDetailBackgroundView addSubview:detailImageView];
    
    UIView *detailBulletPoint = [[UIView alloc] initWithFrame:CGRectMake(detailImageView.frame.origin.x + detailImageView.frame.size.width + 10, (detailImageView.frame.origin.y + (detailImageView.frame.size.height / 2)) - (bulletPointSize / 2), bulletPointSize, bulletPointSize)];
    //circleDot.center = CGPointMake((circleDot.frame.size.width) / 2, (circleDot.frame.size.height) / 2);
    detailBulletPoint.backgroundColor = [UIColor darkGrayColor];
    detailBulletPoint.layer.cornerRadius = detailBulletPoint.frame.size.width / 2;
    [_eventDetailBackgroundView addSubview:detailBulletPoint];
    
    float detailOriginalHeight = 0;
    
    UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(detailBulletPoint.frame.origin.x + detailBulletPoint.frame.size.width + 10, detailImageView.frame.origin.y,(_eventDetailBackgroundView.frame.size.width - 20) - (detailBulletPoint.frame.origin.x + detailBulletPoint.frame.size.width + 10), detailImageView.frame.size.height)];
    
    detailOriginalHeight = detailLabel.frame.size.height;
    
    //detailLabel.backgroundColor = [UIColor redColor];
    detailLabel.numberOfLines = 0;
    //detailLabel.text = @"Club Answer, Seoul, SK Club Answer, Seoul";
    detailLabel.text = [NSString stringWithFormat:@"%@",_event.eventDescription];
    detailLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    [detailLabel sizeToFit];
    [_eventDetailBackgroundView addSubview:detailLabel];
    
    if(detailLabel.frame.size.height < detailOriginalHeight){
        
        detailLabel.frame = CGRectMake(detailLabel.frame.origin.x, detailLabel.frame.origin.y, detailLabel.frame.size.width, detailOriginalHeight);
    }else if(detailLabel.frame.size.height > (detailOriginalHeight * 2)){
    
        detailLabel.frame = CGRectMake(detailLabel.frame.origin.x, detailLabel.frame.origin.y - 10, detailLabel.frame.size.width, detailLabel.frame.size.height);
    }
    
    gradientBulletPoint.frame = CGRectMake(detailBulletPoint.frame.origin.x - ((gradientBulletPoint.frame.size.width / 2) - (detailBulletPoint.frame.size.width / 2)), detailLabel.frame.origin.y + detailLabel.frame.size.height + gapBetweenBulletPoint - ((gradientBulletPoint.frame.size.height) / 9), gradientBulletPoint.frame.size.width, gradientBulletPoint.frame.size.height);
    
    whiteBulletPoint.frame = CGRectMake(detailBulletPoint.frame.origin.x - ((whiteBulletPoint.frame.size.width / 2) - (detailBulletPoint.frame.size.width / 2)), detailLabel.frame.origin.y + detailLabel.frame.size.height + gapBetweenBulletPoint - ((whiteBulletPoint.frame.size.height / 6) / 2), whiteBulletPoint.frame.size.width, whiteBulletPoint.frame.size.height);
    
    UIView *slotBulletPoint = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (bulletPointSize * 5) - 0, (bulletPointSize * 5) - 0)];
    slotBulletPoint.center = CGPointMake(((bulletPointSize * 5) / 2) - 0, ((bulletPointSize * 5) / 2) - 0);
    slotBulletPoint.backgroundColor = [UIColor darkGrayColor];
    slotBulletPoint.layer.cornerRadius = slotBulletPoint.frame.size.width / 2;
    [_eventDetailBackgroundView addSubview:slotBulletPoint];
    
    UILabel *slotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, slotBulletPoint.frame.size.width, slotBulletPoint.frame.size.height)];
    //slotLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //slotLabel.text = @"15";
    slotLabel.text = [NSString stringWithFormat:@"%@",_event.remainingSlots];
    slotLabel.textAlignment = NSTextAlignmentCenter;
    slotLabel.font = [UIFont fontWithName:@"Futura ICG" size:35];
    slotLabel.textColor = [UIColor whiteColor];
    [slotBulletPoint addSubview:slotLabel];
    
    slotBulletPoint.frame = CGRectMake(detailBulletPoint.frame.origin.x - ((slotBulletPoint.frame.size.width / 2) - (detailBulletPoint.frame.size.width / 2)), detailLabel.frame.origin.y + detailLabel.frame.size.height + gapBetweenBulletPoint, slotBulletPoint.frame.size.width, slotBulletPoint.frame.size.height);
    
    connectedBulletPointLine.frame = CGRectMake(connectedBulletPointLine.frame.origin.x, connectedBulletPointLine.frame.origin.y, connectedBulletPointLine.frame.size.width, slotBulletPoint.frame.origin.y);
    
    UILabel *remainingLabel = [[UILabel alloc] initWithFrame:CGRectMake(slotBulletPoint.frame.origin.x + slotBulletPoint.frame.size.width + 15, slotBulletPoint.frame.origin.y, _eventDetailBackgroundView.frame.size.width / 2, slotBulletPoint.frame.size.height)];
    //slotLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    remainingLabel.text = @"Remaining Slots";
    remainingLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    remainingLabel.textColor = [UIColor darkGrayColor];
    [_eventDetailBackgroundView addSubview:remainingLabel];
    
    NSMutableArray *itemToHide = [[NSMutableArray alloc] init];
    
    UILabel *headerAddInvitesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, remainingLabel.frame.origin.y + remainingLabel.frame.size.height + 40, _eventDetailBackgroundView.frame.size.width, 30)];
    //headerAddInvitesLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    headerAddInvitesLabel.text = @"Introduce a new Insider to the community?";
    headerAddInvitesLabel.textAlignment = NSTextAlignmentCenter;
    headerAddInvitesLabel.font = [UIFont fontWithName:@"Futura ICG" size:10];
    [_eventDetailBackgroundView addSubview:headerAddInvitesLabel];
    
    [itemToHide addObject:headerAddInvitesLabel];
    
    UIButton *decreaseButton = [UIButton buttonWithType:UIButtonTypeSystem];
    decreaseButton.frame = CGRectMake((headerAddInvitesLabel.frame.size.width / 2) - (30 * 2), headerAddInvitesLabel.frame.origin.y + headerAddInvitesLabel.frame.size.height + 10, 30, 30);
    decreaseButton.backgroundColor = [UIColor whiteColor];
    decreaseButton.layer.borderColor = [UIColor grayColor].CGColor;
    decreaseButton.layer.borderWidth = 1;
    decreaseButton.tag = 0;
    [decreaseButton setTitle:@"-" forState:UIControlStateNormal];
    [decreaseButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_eventDetailBackgroundView addSubview:decreaseButton];
    
    [decreaseButton addTarget:self action:@selector(inviteEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [itemToHide addObject:decreaseButton];
    
    UIButton *increaseButton = [UIButton buttonWithType:UIButtonTypeSystem];
    increaseButton.frame = CGRectMake((headerAddInvitesLabel.frame.size.width / 2) + 30, headerAddInvitesLabel.frame.origin.y + headerAddInvitesLabel.frame.size.height + 10, 30, 30);
    increaseButton.backgroundColor = [UIColor whiteColor];
    increaseButton.layer.borderColor = [UIColor grayColor].CGColor;
    increaseButton.layer.borderWidth = 1;
    increaseButton.tag = 1;
    [increaseButton setTitle:@"+" forState:UIControlStateNormal];
    [increaseButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_eventDetailBackgroundView addSubview:increaseButton];
    
    [increaseButton addTarget:self action:@selector(inviteEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [itemToHide addObject:increaseButton];
    
    _invitedCounterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, increaseButton.frame.origin.y, _eventDetailBackgroundView.frame.size.width, 30)];
    //additionalLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    _invitedCounterLabel.text = @"0";
    _invitedCounterLabel.textColor = [UIColor grayColor];
    _invitedCounterLabel.userInteractionEnabled = NO;
    _invitedCounterLabel.textAlignment = NSTextAlignmentCenter;
    _invitedCounterLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_eventDetailBackgroundView addSubview:_invitedCounterLabel];
    
    [itemToHide addObject:_invitedCounterLabel];
    
    _rsvpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _rsvpButton.frame = CGRectMake(20, _invitedCounterLabel.frame.origin.y + _invitedCounterLabel.frame.size.height + 30, _eventDetailBackgroundView.frame.size.width - 40, 40);
    _rsvpButton.backgroundColor = [UIColor darkGrayColor];
    _rsvpButton.layer.cornerRadius = 8;
    _rsvpButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    
    NSLog(@"hasRsvped = %i | _isViewingOnly = %i",_event.hasRsvped,_isViewingOnly);
    
    if(_event.hasRsvped && _isViewingOnly == false){
    
        _rsvpButton.userInteractionEnabled = NO;
        _rsvpButton.backgroundColor = [UIColor grayColor];
        [_rsvpButton setTitle:@"INVITE" forState:UIControlStateNormal];
    }else if(_isViewingOnly || _event.remainingAdditionalGuests <= 0){
    
        for (int i = 0; i < itemToHide.count; i++) {
            
            id object = [itemToHide objectAtIndex:i];
            [object removeFromSuperview];
            object = nil;
        }
        
        if(_event.remainingAdditionalGuests == _event.maxAddGuest){
        
            _rsvpButton.userInteractionEnabled = NO;
            _rsvpButton.backgroundColor = [UIColor grayColor];
        }else{
        
            _rsvpButton.userInteractionEnabled = YES;
        }
        
        [_rsvpButton setTitle:@"VIEW INVITATION" forState:UIControlStateNormal];
    }else{
    
        [_rsvpButton setTitle:@"RSVP" forState:UIControlStateNormal];
    }

    if(!_isDisableInviteAndRSVP){
        
        [_rsvpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_eventDetailBackgroundView addSubview:_rsvpButton];
    }
    
    [_rsvpButton addTarget:self action:@selector(rsvpEvent) forControlEvents:UIControlEventTouchUpInside];
    
    _eventDetailBackgroundView.frame = CGRectMake(_eventDetailBackgroundView.frame.origin.x, _eventDetailBackgroundView.frame.origin.y, _eventDetailBackgroundView.frame.size.width, _rsvpButton.frame.origin.y + _rsvpButton.frame.size.height + 30);
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _navigationBarHeight + _eventDetailBackgroundView.frame.origin.y + _eventDetailBackgroundView.frame.size.height + 10);
}

-(void)rsvpEvent{
    
    [self.view endEditing:YES];
        
    NSMutableArray *invitedArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _invitedArray.count; i++) {
        
        AdditionalInvite *ai = [_invitedArray objectAtIndex:i];
        
        NSDictionary *guestName = @{ @"name" : ai.invitedFullName,
                                     @"salutation" : [ai.salutationLabel.text stringByReplacingOccurrencesOfString:@"." withString:@""] };
        
        [invitedArray addObject:guestName];
    }

    if(_event.hasRsvped && _isViewingOnly == false){
    
        [_ls loadingScreen:^{
           
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                [_api inviteGuestsWithToken:_uc.authenticationToken eventId:_event.eventId invitedGuests:[invitedArray copy] isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                    
                    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                    
                    NSLog(@"invite guest json = %@", jsonDictionary);
                    
                    InviteGuests *ig = [[InviteGuests alloc] initWithDictionary:jsonDictionary];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        
                        if(isComplete){
                            
                            UIAlertController *alertControllerGuests = [UIAlertController alertControllerWithTitle:@"Slot Confirmed" message:ig.message preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* closeGuests = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                
                                if(ig.status){
                                    
                                    RsvpViewController *rsvpVC = [[RsvpViewController alloc] init];
                                    rsvpVC.eventId = _eventId;
                                    [self.navigationController pushViewController:rsvpVC animated:YES];
                                    
                                    NSLog(@"success invite guests");
                                }
                            }];
                            
                            [alertControllerGuests addAction:closeGuests];
                            
                            [self presentViewController:alertControllerGuests animated:YES completion:nil];
                            
                        }else{
                            
                            NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                            NSLog(@"failed to load json at invite guests");
                        }
                        
                        [_ls finishLoading];
                    });
                }];
            });
        }];
    }else if(_isViewingOnly){
        
        [_ls loadingScreen:^{
           
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                [_api getViewInvitationsWithToken:_uc.authenticationToken eventId:_event.eventId isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                    
                    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                    
                    NSLog(@"invite guest json = %@", jsonDictionary);
                    
                    ViewInvitations *vi = [[ViewInvitations alloc] initWithDictionary:jsonDictionary];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        
                        if(isComplete){
                            
                            if(!vi.status){
                                
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:vi.message preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                                [alertController addAction:close];
                                [self presentViewController:alertController animated:YES completion:nil];
                            }else{
                                
                                RsvpViewController *rsvpVC = [[RsvpViewController alloc] init];
                                rsvpVC.eventId = _eventId;
                                [self.navigationController pushViewController:rsvpVC animated:YES];
                                
                                NSLog(@"success view invitations");
                            }
                        }else{
                            
                            NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                            NSLog(@"failed to load json at view invitations");
                        }
                        
                        [_ls finishLoading];
                    });
                }];
            });
        }];
    }else{
    
        [_ls loadingScreen:^{
           
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                [_api rsvpWithToken:_uc.authenticationToken eventId:_event.eventId invitedGuests:[invitedArray copy] isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                    
                    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                    
                    NSLog(@"rsvp json = %@", jsonDictionary);
                    
                    RSVP *rsvp = [[RSVP alloc] initWithDictionary:jsonDictionary];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        
                        if(isComplete){
                            
                            if(!rsvp.status){
                                
                                UIAlertController *alertControllerRsvp = [UIAlertController alertControllerWithTitle:@"" message:rsvp.messageRsvp preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction* closeRsvp = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                    
                                    UIAlertController *alertControllerGuests = [UIAlertController alertControllerWithTitle:@"" message:rsvp.messageGuests preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction* closeGuests = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                                    
                                    [alertControllerGuests addAction:closeGuests];
                                    
                                    [self presentViewController:alertControllerGuests animated:YES completion:nil];
                                }];
                                
                                [alertControllerRsvp addAction:closeRsvp];
                                
                                [self presentViewController:alertControllerRsvp animated:YES completion:nil];
                            }else{
                                
                                UIAlertController *alertControllerRsvp = [UIAlertController alertControllerWithTitle:@"Slot Confirmed" message:rsvp.messageRsvp preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction* closeRsvp = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                    
                                    if(_invitedArray.count != 0){
                                        
                                        UIAlertController *alertControllerGuests = [UIAlertController alertControllerWithTitle:@"" message:rsvp.messageGuests preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction* closeGuests = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                            
                                            if(rsvp.statusGuests){
                                                
                                                RsvpViewController *rsvpVC = [[RsvpViewController alloc] init];
                                                rsvpVC.eventId = _eventId;
                                                [self.navigationController pushViewController:rsvpVC animated:YES];
                                                
                                                NSLog(@"success rsvp with guests");
                                            }else{
                                                
                                                [self refreshEventPage];
                                            }
                                        }];
                                        
                                        [alertControllerGuests addAction:closeGuests];
                                        
                                        [self presentViewController:alertControllerGuests animated:YES completion:nil];
                                    }else{
                                    
                                        [self refreshEventPage];
                                    }
                                }];
                                
                                [alertControllerRsvp addAction:closeRsvp];
                                
                                [self presentViewController:alertControllerRsvp animated:YES completion:nil];
                                
                                NSLog(@"success rsvp");
                            }
                        }else{
                            
                            NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                            
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                            [alertController addAction:close];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                            NSLog(@"failed to load json at rsvp");
                        }
                        
                        [_ls finishLoading];
                    });
                }];
            });
        }];
    }
}

-(void)refreshEventPage{

    NSLog(@"refresh page");
    
    [UIView animateWithDuration: 0.15
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         _scrollView.alpha = 0.0;
                     }completion:^(BOOL complete){
                         
                         [_invitedArray removeAllObjects];
                         [_scrollView removeFromSuperview];
                         _scrollView = nil;
                         [self viewWillAppear:YES];
                     }];
}

-(void)inviteEvent:(UIButton*)sender{
    
    if(sender.tag == 0){
    
        NSLog(@"decrease invite");
        
        if(_invitedArray.count != 0){
        
            __block AdditionalInvite *deletedAi = [_invitedArray lastObject];
            
            [UIView animateWithDuration: 0.25
                                  delay: 0.0
                                options: UIViewAnimationOptionCurveEaseInOut
                             animations: ^{
                                 
                                 deletedAi.alpha = 0.0;
                                 
                                 _rsvpButton.frame = CGRectMake(_rsvpButton.frame.origin.x, deletedAi.frame.origin.y, _rsvpButton.frame.size.width, _rsvpButton.frame.size.height);
                                 
                                 _eventDetailBackgroundView.frame = CGRectMake(_eventDetailBackgroundView.frame.origin.x, _eventDetailBackgroundView.frame.origin.y, _eventDetailBackgroundView.frame.size.width, _rsvpButton.frame.origin.y + _rsvpButton.frame.size.height + 30);
                                 
                                 _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _eventDetailBackgroundView.frame.origin.y + _eventDetailBackgroundView.frame.size.height + 30 + 40);
                                 
                             }completion:^(BOOL complete){
                                 
                                 [_invitedArray removeLastObject];
                                 
                                 [deletedAi removeFromSuperview];
                                 deletedAi = nil;
                                 
                                 _invitedCounterLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)_invitedArray.count];
                                 
                                 if(_invitedArray.count == 0){
                                 
                                     [UIView animateWithDuration: 0.25
                                                           delay: 0.0
                                                         options: UIViewAnimationOptionCurveEaseInOut
                                                      animations: ^{
                                                          
                                                          if(_event.hasRsvped){
                                                          
                                                              _rsvpButton.backgroundColor = [UIColor grayColor];
                                                              _rsvpButton.userInteractionEnabled = NO;
                                                          }
                                                      }completion:nil];
                                 }
                                 
                                 NSLog(@"all invites : %lu",(unsigned long)_invitedArray.count);
                             }];
        }
        
    }else if(sender.tag == 1){
    
        NSLog(@"increase invite");
        
        if((_invitedArray.count >= [_event.remainingSlots integerValue]) || (_invitedArray.count == [_event.remainingAdditionalGuests integerValue])){
        
            NSLog(@"exceeding to remaining slots or remaining additional guests");
        }else{
            
            AdditionalInvite *ai;
            
            if(_invitedArray.count == 0){
                
                ai = [[AdditionalInvite alloc] initWithFrame:CGRectMake(0, _invitedCounterLabel.frame.origin.y + _invitedCounterLabel.frame.size.height + 30, _eventDetailBackgroundView.frame.size.width, 25)];
                ai.delegate = self;
                ai.alpha = 0.0;
                //ai.backgroundColor = [UIColor greenColor];
                [_eventDetailBackgroundView addSubview:ai];
            }else{
                
                AdditionalInvite *tempAi = [_invitedArray lastObject];
                
                ai = [[AdditionalInvite alloc] initWithFrame:CGRectMake(0, tempAi.frame.origin.y + tempAi.frame.size.height + 10, _eventDetailBackgroundView.frame.size.width, 25)];
                ai.delegate = self;
                ai.alpha = 0.0;
                //ai.backgroundColor = [UIColor greenColor];
                [_eventDetailBackgroundView addSubview:ai];
            }
            
            [UIView animateWithDuration: 0.25
                                  delay: 0.0
                                options: UIViewAnimationOptionCurveEaseInOut
                             animations: ^{
                                 
                                 ai.alpha = 1.0;
                                 
                                 _rsvpButton.frame = CGRectMake(_rsvpButton.frame.origin.x, ai.frame.origin.y + ai.frame.size.height + 30, _rsvpButton.frame.size.width, _rsvpButton.frame.size.height);
                                 
                                 _eventDetailBackgroundView.frame = CGRectMake(_eventDetailBackgroundView.frame.origin.x, _eventDetailBackgroundView.frame.origin.y, _eventDetailBackgroundView.frame.size.width, _rsvpButton.frame.origin.y + _rsvpButton.frame.size.height + 30);
                                 
                                 _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _eventDetailBackgroundView.frame.origin.y + _eventDetailBackgroundView.frame.size.height + ai.frame.size.height + 30 + 20);
                                 
                             }completion:^(BOOL complete){
                                 
                                 [_invitedArray addObject:ai];
                                 
                                 _invitedCounterLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)_invitedArray.count];
                                 
                                 if(_invitedArray.count >= 1){
                                     
                                     [UIView animateWithDuration: 0.25
                                                           delay: 0.0
                                                         options: UIViewAnimationOptionCurveEaseInOut
                                                      animations: ^{
                                                          
                                                          if(_event.hasRsvped){
                                                              
                                                              _rsvpButton.backgroundColor = [UIColor darkGrayColor];
                                                              _rsvpButton.userInteractionEnabled = YES;
                                                          }
                                                      }completion:nil];
                                 }
                                 
                                 NSLog(@"all invites : %lu",(unsigned long)_invitedArray.count);
                             }];
        }
    }
    
    [_scrollView scrollRectToVisible:CGRectMake(_scrollView.frame.origin.x, _scrollView.contentSize.height, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:YES];
}

-(void)AdditionalInviteDidPopUp:(AdditionalInvite *)additionalInvite{

    PickerView *pv = [[PickerView alloc] init];
    pv.delegate = self;
    [self.view addSubview:pv];
    
    [pv showPicker];
    
    _forSalutation = additionalInvite;
}

-(void)AdditionalKeyboardWillShow:(NSNotification *)notification{

    /*
     * Get the height of the keyboard and subtract 30px
     * the 30px is the auto-complete words appear at the
     * top of keyboard
     */
    
    float keyBoardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height - 30;
    
    /*
     * Check if screen is on default position
     * Screen y-axis = 0
     */
    
    if(self.view.frame.origin.y >= 0){
        
        /*
         * Move the screen upward base on the height of the keyboard
         * Screen Y-axis subtract by height of keyboard
         * Animation is done automatically
         */
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - (keyBoardHeight), self.view.frame.size.width, self.view.frame.size.height + 30);
    }
}

-(void)AdditionalKeyboardWillBeHidden:(NSNotification *)notification{

    /*
     * Reposition of screen by moving downward
     * Back to default Y-axis position
     */
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + 30);
}

-(void)pickerViewDidSelectRow:(PickerView *)pickerView salutation:(NSString *)salutation{

    _forSalutation.salutationLabel.text = salutation;
}
@end
