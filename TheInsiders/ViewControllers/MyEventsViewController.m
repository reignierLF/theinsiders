//
//  MyEventsViewController.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 9/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "MyEventsViewController.h"
#import "EventViewController.h"
#import "MenuController.h"

#import "ListEvents.h"

#import "cUIScrollView.h"
#import "ImageLoader.h"
#import "LoadingScreen.h"

#import "Api.h"
#import "UserCredentials.h"
#import "UpcomingEvents.h"
#import "PastEvents.h"

@interface MyEventsViewController () <MenuControllerDelegate, UIScrollViewDelegate, ListEventsDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;

@property (nonatomic, strong) UIView *switchView;

@property (nonatomic, strong) MenuController *mc;
@property (nonatomic, strong) cUIScrollView *scrollview;
@property (nonatomic, strong) UIView *underline;
@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) UserCredentials *uc;

@property (nonatomic, strong) ListEvents *le;

@end

@implementation MyEventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initSwitchLayout];
    [self initUpComingEvents];
    [self initMenuController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _navigationBarHeight = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    self.navigationController.navigationBarHidden = NO;
    self.title = @"My Events";
    
    _api = [[Api alloc] init];
    _uc = [[UserCredentials alloc] init];
    
    _ls = [[LoadingScreen alloc] init];
    //[self.view addSubview:_ls];
}

-(void)initUpComingEvents{
    
    [_ls loadingScreen:^{
       
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api getUpcomingEventsWithToken:_uc.authenticationToken isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"upcoming events json = %@", jsonDictionary);
                
                UpcomingEvents *ue = [[UpcomingEvents alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        _le = [[ListEvents alloc] initWithFrame:CGRectMake(0, _switchView.frame.origin.y + _switchView.frame.size.height, _screenWidth, _screenHeight - (_navigationBarHeight + 40))];
                        _le.delegate = self;
                        _le.size = ue.size;
                        _le.address = ue.address;
                        _le.dateTime = ue.dateTime;
                        _le.eventImageUrl = ue.eventImageUrl;
                        _le.hasInvited = ue.hasInvited;
                        _le.eventId = ue.eventId;
                        _le.title = ue.title;
                        [self.view addSubview:_le];
                        
                        NSLog(@"success upcoming events");
                        
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at upcoming events");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)initPastEvents{
    
    [_ls loadingScreen:^{
       
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api getPastEventsWithToken:_uc.authenticationToken isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"past events json = %@", jsonDictionary);
                
                PastEvents *pa = [[PastEvents alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        _le = [[ListEvents alloc] initWithFrame:CGRectMake(0, _switchView.frame.origin.y + _switchView.frame.size.height, _screenWidth, _screenHeight - (_navigationBarHeight + 40))];
                        _le.delegate = self;
                        _le.size = pa.size;
                        _le.address = pa.address;
                        _le.dateTime = pa.dateTime;
                        _le.eventImageUrl = pa.eventImageUrl;
                        _le.hasInvited = pa.hasInvited;
                        _le.eventId = pa.eventId;
                        _le.title = pa.title;
                        _le.isPastEvent = YES;
                        [self.view addSubview:_le];
                        
                        NSLog(@"success past events");
                        
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load json at past events");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Insider App BG"]];
    backgroundImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initSwitchLayout{
    
    _switchView = [[UIView alloc] initWithFrame:CGRectMake(0, _navigationBarHeight, _screenWidth, 40)];
    //_switchView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:_switchView];
    
    UILabel *upcomingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _screenWidth / 2, _switchView.frame.size.height - 3)];
    //headerLabel.backgroundColor = [UIColor redColor];
    upcomingLabel.text = @"Upcoming Events";
    upcomingLabel.textAlignment = NSTextAlignmentCenter;
    upcomingLabel.textColor = [UIColor whiteColor];
    upcomingLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [_switchView addSubview:upcomingLabel];
    
    UIButton *upcomingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    upcomingButton.frame = upcomingLabel.frame;
    //upcomingButton.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
    upcomingButton.tag = 0;
    [_switchView addSubview:upcomingButton];
    
    [upcomingButton addTarget:self action:@selector(switchEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *pastLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 2, 0, _screenWidth / 2, _switchView.frame.size.height - 3)];
    //headerLabel.backgroundColor = [UIColor redColor];
    pastLabel.text = @"Past Events";
    pastLabel.textAlignment = NSTextAlignmentCenter;
    pastLabel.textColor = [UIColor whiteColor];
    pastLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [_switchView addSubview:pastLabel];
    
    UIButton *pastButton = [UIButton buttonWithType:UIButtonTypeCustom];
    pastButton.frame = pastLabel.frame;
    //pastButton.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
    pastButton.tag = 1;
    [_switchView addSubview:pastButton];
    
    [pastButton addTarget:self action:@selector(switchEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    _underline = [[UIView alloc] initWithFrame:CGRectMake(upcomingButton.frame.origin.x, (upcomingButton.frame.origin.y + upcomingButton.frame.size.height), upcomingButton.frame.size.width, 3)];
    _underline.backgroundColor = [UIColor whiteColor];
    [_switchView addSubview:_underline];
}

-(void)initMenuController{
    
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-(_screenWidth - (_screenWidth / 4)), 0, _screenWidth - (_screenWidth / 2.5), _screenHeight)];
    _mc.delegate = self;
    _mc.menu.settings.menuBackgroundImage = @"Insider App BG";
    _mc.menu.settings.textColor = [UIColor whiteColor];
    _mc.menu.settings.font = [UIFont fontWithName:@"Futura ICG" size:16];
    _mc.menu.settings.linebreakColor = [UIColor clearColor];
    _mc.popToFront = YES;
    _mc.headerSpace = 50;
    _mc.textMargin = 30;
    [self.view addSubview:_mc];
    
    UILabel *userFullNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 60, _mc.frame.size.width - 40, 50)];
    //userFullNameLabel.backgroundColor = [UIColor redColor];
    userFullNameLabel.text = [NSString stringWithFormat:@"%@",_uc.name];
    userFullNameLabel.textAlignment = NSTextAlignmentCenter;
    userFullNameLabel.textColor = [UIColor whiteColor];
    userFullNameLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [_mc addSubview:userFullNameLabel];
}

-(void)switchEvent:(UIButton*)sender{
    
    NSLog(@"switch view");
    
    [UIView animateWithDuration: 0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         _underline.alpha = 0.5;
                         
                         _underline.frame = CGRectMake(sender.frame.origin.x, _underline.frame.origin.y, _underline.frame.size.width, _underline.frame.size.height);
                         
                         _le.alpha = 0.0;
                         
                     }completion:^(BOOL finished){
                         
                         if(finished){
                             
                             [_le removeFromSuperview];
                             _le = nil;
                             
                             if(sender.tag == 0){
                             
                                 [self initUpComingEvents];
                             }else if(sender.tag == 1){
                             
                                 [self initPastEvents];
                             }
                             
                             [UIView animateWithDuration: 0.3
                                                   delay: 0.0
                                                 options: UIViewAnimationOptionCurveEaseInOut
                                              animations: ^{
                                                  
                                                  _underline.alpha = 1.0;
                                                  
                                              }completion:nil];
                         }
                     }];
}

-(void)didSelectEvent:(ListEvents *)eventsCell sender:(UIButton *)sender{

    EventViewController *evc = [[EventViewController alloc] init];
    evc.index = sender.tag;
    evc.isViewingOnly = YES;
    evc.eventId = [_le.eventId objectAtIndex:sender.tag];
    evc.isDisableInviteAndRSVP = eventsCell.isPastEvent;
    [self.navigationController pushViewController:evc animated:YES];
}

-(void)didRefreshCurrentViewController{
    
}

-(void)didSelectMenu:(MenuController *)menuController viewController:(UIViewController *)viewController{
    
    NSLog(@"viewController = %@",viewController);
    
    if([viewController.title isEqualToString:@"Sign Out"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sign out" message:@"Are you sure you want to sign out?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* signout = [UIAlertAction actionWithTitle:@"Sign out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.view.clipsToBounds = YES;
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        [alertController addAction:signout];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [self.navigationController pushViewController:viewController animated:NO];
            
            [UIView transitionFromView:self.view
                                toView:viewController.view
                              duration:0.25
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            completion:^(BOOL isComplete){
                                
                                [self garbageCollector:viewController];
                            }];
        });
    }
}

-(void)didSelectSubMenu:(MenuController *)menuController{
    
    NSLog(@"Did select sub menu");
}

-(void)garbageCollector:(UIViewController*)vc{
    
    _mc.menu.viewControllers = nil;
    [_mc.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _mc = nil;
    
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if(vc != nil){
        
        NSMutableArray *viewControllerArray = [self.navigationController.viewControllers mutableCopy];
        
        UIViewController *uv = [viewControllerArray objectAtIndex:1];
        
        [uv.view removeFromSuperview];
        uv = nil;
        
        [viewControllerArray removeObjectAtIndex:1];
        
        vc.navigationController.viewControllers = viewControllerArray;
    }
}
@end
