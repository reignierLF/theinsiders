//
//  Login.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Login : NSObject

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;

@property (nonatomic, readonly) BOOL active;
@property (nonatomic, readonly) NSString *authenticationToken;
@property (nonatomic, readonly) NSString *dob;
@property (nonatomic, readonly) NSString *email;
@property (nonatomic, readonly) NSString *gender;
@property (nonatomic, readonly) NSString *loginId;
@property (nonatomic, readonly) NSString *mobileNo;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *qrCodeInfoUrl;
@property (nonatomic, readonly) NSString *salutation;
@property (nonatomic, readonly) NSString *signInCount;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
