//
//  UserCredentials.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 17/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "UserCredentials.h"

@interface UserCredentials ()

@property (nonatomic, strong) NSString *authenticationTokenKey;
@property (nonatomic, strong) NSString *dobKey;
@property (nonatomic, strong) NSString *emailKey;
@property (nonatomic, strong) NSString *genderKey;
@property (nonatomic, strong) NSString *userIdKey;
@property (nonatomic, strong) NSString *mobileNoKey;
@property (nonatomic, strong) NSString *nameKey;
@property (nonatomic, strong) NSString *qrCodeInfoUrlKey;
@property (nonatomic, strong) NSString *salutationKey;
@property (nonatomic, strong) NSString *signInCountKey;
@property (nonatomic, strong) NSString *loginAsKey;

@end

@implementation UserCredentials

-(instancetype)init{

    self = [super init];
    
    if(self){
    
        /*
        [self setAuthenticationToken:@""];
        [self setDob:@""];
        [self setEmail:@""];
        [self setGender:@""];
        [self setUserId:@""];
        [self setMobileNo:@""];
        [self setName:@""];
        [self setQrCodeInfoUrl:@""];
        [self setSalutation:@""];
        [self setSignInCount:@""];
        */
        
        _authenticationTokenKey =   @"authenticationToken";
        _dobKey =                   @"dob";
        _emailKey =                 @"email";
        _genderKey =                @"gender";
        _userIdKey =                @"userId";
        _mobileNoKey =              @"mobileNo";
        _nameKey =                  @"name";
        _qrCodeInfoUrlKey =         @"qrCodeInfoUrl";
        _salutationKey =            @"salutation";
        _signInCountKey =           @"signInCount";
        _loginAsKey =               @"loginAs";
    }
    
    return self;
}

-(void)setAuthenticationToken:(NSString *)authenticationToken{

    [[NSUserDefaults standardUserDefaults] setObject:authenticationToken forKey:_authenticationTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)authenticationToken{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_authenticationTokenKey];
}

-(void)setDob:(NSString *)dob{

    [[NSUserDefaults standardUserDefaults] setObject:dob forKey:_dobKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)dob{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_dobKey];
}

-(void)setEmail:(NSString *)email{

    [[NSUserDefaults standardUserDefaults] setObject:email forKey:_emailKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)email{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_emailKey];
}

-(void)setGender:(NSString *)gender{

    [[NSUserDefaults standardUserDefaults] setObject:gender forKey:_genderKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)gender{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_genderKey];
}

-(void)setUserId:(NSString *)userId{

    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:_userIdKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)userId{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_userIdKey];
}

-(void)setMobileNo:(NSString *)mobileNo{

    [[NSUserDefaults standardUserDefaults] setObject:mobileNo forKey:_mobileNoKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)mobileNo{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_mobileNoKey];
}

-(void)setName:(NSString *)name{
    
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:_nameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)name{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:_nameKey];
}

-(void)setQrCodeInfoUrl:(NSString *)qrCodeInfoUrl{
    
    [[NSUserDefaults standardUserDefaults] setObject:qrCodeInfoUrl forKey:_qrCodeInfoUrlKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)qrCodeInfoUrl{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:_qrCodeInfoUrlKey];
}

-(void)setSalutation:(NSString *)salutation{
    
    [[NSUserDefaults standardUserDefaults] setObject:salutation forKey:_salutationKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)salutation{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_salutationKey];
}

-(void)setSignInCount:(NSString *)signInCount{

    [[NSUserDefaults standardUserDefaults] setObject:signInCount forKey:_signInCountKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)signInCount{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_signInCountKey];
}

-(void)setLoginAs:(NSString *)loginAs{

    [[NSUserDefaults standardUserDefaults] setObject:loginAs forKey:_loginAsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)loginAs{

    return [[NSUserDefaults standardUserDefaults] objectForKey:_loginAsKey];
}
@end
