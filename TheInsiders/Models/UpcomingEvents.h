//
//  UpcomingEvents.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpcomingEvents : NSObject

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;

@property (nonatomic, readonly) NSInteger size;
@property (nonatomic, readonly) NSArray *address;
@property (nonatomic, readonly) NSArray *dateTime;
@property (nonatomic, readonly) NSArray *eventImageUrl;
@property (nonatomic, readonly) NSArray *hasInvited;
@property (nonatomic, readonly) NSArray *eventId;
@property (nonatomic, readonly) NSArray *title;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
