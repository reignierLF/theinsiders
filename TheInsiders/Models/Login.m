//
//  Login.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "Login.h"

@implementation Login

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _status =               [[self checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message =              [self checkForNull:[dic valueForKey:@"message"]];
        
        NSDictionary *dataDic = [dic valueForKey:@"data"];
        
        _active =               [[self checkForNull:[dataDic valueForKey:@"active"]] boolValue];
        _authenticationToken =  [self checkForNull:[dataDic valueForKey:@"authentication_token"]];
        _dob =                  [self checkForNull:[dataDic valueForKey:@"date_of_birth"]];
        _email =                [self checkForNull:[dataDic valueForKey:@"email"]];
        _gender =               [self checkForNull:[dataDic valueForKey:@"gender_desc"]];
        _loginId =              [self checkForNull:[dataDic valueForKey:@"id"]];
        _mobileNo =             [self checkForNull:[dataDic valueForKey:@"mobile_no"]];
        _name =                 [self checkForNull:[dataDic valueForKey:@"name"]];
        _qrCodeInfoUrl =        [self checkForNull:[dataDic valueForKey:@"qr_code_info_url"]];
        _salutation =           [self checkForNull:[dataDic valueForKey:@"salutation"]];
        _signInCount =          [self checkForNull:[dataDic valueForKey:@"sign_in_count"]];
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
    
        object = @"";
    }

    return object;
}
@end
