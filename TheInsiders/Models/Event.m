//
//  Event.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "Event.h"

@implementation Event

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _status =                       [[self checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message =                      [self checkForNull:[dic valueForKey:@"message"]];
        
        NSDictionary *dataDic =         [dic valueForKey:@"data"];
    
        _address =                      [self checkForNull:[dataDic valueForKey:@"address"]];
        _dateTime =                     [self checkForNull:[dataDic valueForKey:@"date_time"]];
        _eventDescription =             [self checkForNull:[dataDic valueForKey:@"event_description"]];
        _eventImageUrl =                [self checkForNull:[dataDic valueForKey:@"event_image_url"]];
        _hasRsvped =                    [[self checkForNull:[dataDic valueForKey:@"has_rsvped"]] boolValue];
        _eventId =                      [self checkForNull:[dataDic valueForKey:@"id"]];
        _maxAddGuest =                  [self checkForNull:[dataDic valueForKey:@"max_add_guests"]];
        _remainingAdditionalGuests =    [self checkForNull:[dataDic valueForKey:@"remaining_additional_guests"]];
        _remainingSlots =               [self checkForNull:[dataDic valueForKey:@"remaining_slots"]];
        _title =                        [self checkForNull:[dataDic valueForKey:@"title"]];
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}
@end
