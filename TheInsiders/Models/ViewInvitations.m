//
//  ViewInvitations.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "ViewInvitations.h"

@implementation ViewInvitations

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _status =                   [[self checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message =                  [self checkForNull:[dic valueForKey:@"message"]];
        
        NSDictionary *dataDic =     [dic valueForKey:@"data"];
        
        _size = dataDic.count;
        
        if(_size != 0){
            
            _address =              [self checkForNullArray:[dataDic valueForKey:@"address"]];
            _dateTime =             [self checkForNullArray:[dataDic valueForKey:@"date_time"]];
            _inviteDescription =    [self checkForNullArray:[dataDic valueForKey:@"invite_description"]];
            _invitedBy =            [self checkForNullArray:[dataDic valueForKey:@"invited_by"]];
            _name =                 [self checkForNullArray:[dataDic valueForKey:@"name"]];
            _qrCodeUrl =            [self checkForNullArray:[dataDic valueForKey:@"qr_code_url"]];
            _salutation =           [self checkForNullArray:[dataDic valueForKey:@"salutation"]];
            _title =                [self checkForNullArray:[dataDic valueForKey:@"title"]];
            _eventImageUrl =        [self checkForNullArray:[dataDic valueForKey:@"event_image_url"]];
            
        }
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}

-(NSArray*)checkForNullArray:(id)object{
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _size; i++) {
        
        if([object objectAtIndex:i] == (id)[NSNull null] || object == nil){
            
            [array addObject:@""];
        }else{
        
            [array addObject:[object objectAtIndex:i]];
        }
    }
    
    return array;
}
@end
