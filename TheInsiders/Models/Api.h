//
//  Api.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^jsonResult)(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse);

@interface Api : NSObject

@property (nonatomic, readonly) NSString *url;

-(void)activationCode:(NSString*)code
           isComplete:(jsonResult)check;

-(void)signUpWithEmail:(NSString*)email
              password:(NSString*)password
             firstName:(NSString*)fName
              lastName:(NSString*)lName
                   dob:(NSString*)dob
              mobileNo:(NSString*)mobileNo
        activationCode:(NSString*)activationCode
            isComplete:(jsonResult)check;

-(void)loginWithEmail:(NSString*)email
             password:(NSString*)password
           isComplete:(jsonResult)check;

-(void)logoutWithToken:(NSString*)token
                userId:(NSString*)userId
            isComplete:(jsonResult)check;

-(void)linkedInSignUpWithEmail:(NSString*)email
                     firstName:(NSString*)firstName
                linkedInUserId:(NSString*)linkedInUserId
                      lastName:(NSString*)lastName
                activationCode:(NSString*)activationCode
                    isComplete:(jsonResult)check;

-(void)linkedInLoginWithEmail:(NSString*)email
               linkedInUserId:(NSString*)linkedInUserId
                   isComplete:(jsonResult)check;

-(void)getAllEventsWithToken:(NSString*)token
                  isComplete:(jsonResult)check;

-(void)getUpcomingEventsWithToken:(NSString*)token
                       isComplete:(jsonResult)check;

-(void)getPastEventsWithToken:(NSString*)token
                   isComplete:(jsonResult)check;

-(void)getEventWithToken:(NSString*)token
                 eventId:(NSString*)eventId
              isComplete:(jsonResult)check;

-(void)getProfileWithToken:(NSString*)token
                isComplete:(jsonResult)check;

-(void)changePaswordWithToken:(NSString*)token
              currentPassword:(NSString*)currentPassword
                  newPassword:(NSString*)newPassword
                   isComplete:(jsonResult)check;

-(void)forgotPasswordWithEmail:(NSString*)email
                    isComplete:(jsonResult)check;

-(void)updateSalutationWithToken:(NSString*)token
                      salutation:(NSString*)salutation
                         loginId:(NSString*)loginId
                      isComplete:(jsonResult)check;

-(void)rsvpWithToken:(NSString*)token
             eventId:(NSString*)eventId
       invitedGuests:(NSArray*)invitedGuests
          isComplete:(jsonResult)check;

-(void)inviteGuestsWithToken:(NSString*)token
                     eventId:(NSString*)eventId
               invitedGuests:(NSArray*)invitedGuests
                  isComplete:(jsonResult)check;

-(void)getViewInvitationsWithToken:(NSString*)token
                           eventId:(NSString*)eventId
                        isComplete:(jsonResult)check;

-(void)getIcebreakerWithToken:(NSString*)token
                   isComplete:(jsonResult)check;

-(void)getRedeemWithToken:(NSString*)token
               isComplete:(jsonResult)check;
@end
