//
//  ViewInvitations.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewInvitations : NSObject

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;

@property (nonatomic, readonly) NSInteger size;
@property (nonatomic, readonly) NSArray *address;
@property (nonatomic, readonly) NSArray *dateTime;
@property (nonatomic, readonly) NSArray *inviteDescription;
@property (nonatomic, readonly) NSArray *invitedBy;
@property (nonatomic, readonly) NSArray *name;
@property (nonatomic, readonly) NSArray *qrCodeUrl;
@property (nonatomic, readonly) NSArray *salutation;
@property (nonatomic, readonly) NSArray *title;
@property (nonatomic, readonly) NSArray *eventImageUrl;

-(instancetype)initWithDictionary:(NSDictionary*)dic;
@end
