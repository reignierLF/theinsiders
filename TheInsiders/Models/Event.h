//
//  Event.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;

@property (nonatomic, readonly) NSString *address;
@property (nonatomic, readonly) NSString *dateTime;
@property (nonatomic, readonly) NSString *eventDescription;
@property (nonatomic, readonly) NSString *eventImageUrl;
@property (nonatomic, readonly) BOOL hasRsvped;
@property (nonatomic, readonly) NSString *eventId;
@property (nonatomic, readonly) NSString *maxAddGuest;
@property (nonatomic, readonly) NSString *remainingAdditionalGuests;
@property (nonatomic, readonly) NSString *remainingSlots;
@property (nonatomic, readonly) NSString *title;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
