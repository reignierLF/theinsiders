//
//  RSVP.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "RSVP.h"

@implementation RSVP

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _status =               [[self checkForNull:[dic valueForKey:@"status"]] boolValue];
        _statusGuests =        [[self checkForNull:[dic valueForKey:@"status_guests"]] boolValue];
        _messageRsvp =          [self checkForNull:[dic valueForKey:@"message_rsvp"]];
        _messageGuests =        [self checkForNull:[dic valueForKey:@"message_guests"]];
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}
@end
