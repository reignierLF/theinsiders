//
//  Logout.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 18/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "Logout.h"

@implementation Logout

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _status =       [[self checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message =      [self checkForNull:[dic valueForKey:@"message"]];
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}
@end
