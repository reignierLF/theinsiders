//
//  Icebreaker.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 2/2/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Icebreaker : NSObject

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;

@property (nonatomic, readonly) NSString *icebreakerTitle;
@property (nonatomic, readonly) NSString *icebreakerDescription;
@property (nonatomic, readonly) NSString *icebreakerImageUrl;
@property (nonatomic, readonly) BOOL isRedeemed;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
