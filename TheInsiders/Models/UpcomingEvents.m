//
//  UpcomingEvents.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "UpcomingEvents.h"

@implementation UpcomingEvents

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _status =               [[self checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message =              [self checkForNull:[dic valueForKey:@"message"]];
        
        NSDictionary *dataDic = [dic valueForKey:@"data"];
        
        _size = dataDic.count;
        
        if(_size != 0){
            
            _address =          [self checkForNullArray:[dataDic valueForKey:@"address"]];
            _dateTime =         [self checkForNullArray:[dataDic valueForKey:@"date_time"]];
            _eventImageUrl =    [self checkForNullArray:[dataDic valueForKey:@"event_image_url"]];
            _hasInvited =       [self checkForNullArray:[dataDic valueForKey:@"has_invited"]];
            _eventId =          [self checkForNullArray:[dataDic valueForKey:@"id"]];
            _title =            [self checkForNullArray:[dataDic valueForKey:@"title"]];
        }
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}

-(NSArray*)checkForNullArray:(id)object{
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _size; i++) {
        
        if([object objectAtIndex:i] == (id)[NSNull null] || object == nil){
            
            [array addObject:@""];
        }else{
            
            [array addObject:[object objectAtIndex:i]];
        }
    }
    
    return array;
}
@end
