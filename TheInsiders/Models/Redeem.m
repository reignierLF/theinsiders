//
//  Redeem.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 2/2/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "Redeem.h"

@implementation Redeem

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _status =                   [[self checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message =                  [self checkForNull:[dic valueForKey:@"message"]];
        
        NSDictionary *dataDic =     [dic valueForKey:@"data"];
        
        _title =                    [self checkForNull:[dataDic valueForKey:@"title"]];
        _icebreakerDescription =    [self checkForNull:[dataDic valueForKey:@"description"]];
        _icebreakerImageUrl =       [self checkForNull:[dataDic valueForKey:@"icebreaker_image_url"]];
        _isRedeemed =               [[self checkForNull:[dic valueForKey:@"is_redeemed"]] boolValue];
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}

@end
