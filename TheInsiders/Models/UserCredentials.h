//
//  UserCredentials.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 17/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserCredentials : NSObject

@property (nonatomic, strong) NSString *authenticationToken;
@property (nonatomic, strong) NSString *dob;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *mobileNo;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *qrCodeInfoUrl;
@property (nonatomic, strong) NSString *salutation;
@property (nonatomic, strong) NSString *signInCount;
@property (nonatomic, strong) NSString *loginAs;

@end
