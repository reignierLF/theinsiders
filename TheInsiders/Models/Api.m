//
//  Api.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 16/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "Api.h"

@implementation Api

-(instancetype)init{

    self = [super init];
    
    if(self){
    
        _url = @"https://the-insiders-dev.herokuapp.com/api/v1/";
        //_url = @"http://192.168.1.2:3000/api/v1/";
    }
    
    return self;
}

-(void)activationCode:(NSString*)code
           isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"ab04ac60-f359-7e80-d686-9e5dd784585c" };
    
    NSDictionary *parameters = @{ @"data": @{ @"activation_code": code } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@activate",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at activation code : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at activation code : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)signUpWithEmail:(NSString*)email
              password:(NSString*)password
             firstName:(NSString*)fName
              lastName:(NSString*)lName
                   dob:(NSString*)dob
              mobileNo:(NSString*)mobileNo
        activationCode:(NSString*)activationCode
            isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"7c9bcb03-fe6d-9ddd-0a3b-938706f20fda" };
    
    NSDictionary *parameters = @{ @"data": @{ @"email":             email,
                                              @"password":          password,
                                              @"first_name":        fName,
                                              @"last_name":         lName,
                                              @"dob":               dob,
                                              @"mobile_no":         mobileNo,
                                              @"activation_code":   activationCode } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@signup",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at signup : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at signup : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)loginWithEmail:(NSString*)email
             password:(NSString*)password
           isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"c96ebc9d-421f-0b4c-da60-dcc1f628f336" };
    NSDictionary *parameters = @{ @"data": @{ @"email":     email,
                                              @"password":  password } };
    //NSDictionary *parameters = @{ @"data": @{ @"email": @"reignier@absolute-living.com", @"password": @"password12345" } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@login",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at login : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at login : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)logoutWithToken:(NSString*)token
                userId:(NSString*)userId
            isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"6fd7883e-24c1-5cf7-26ae-4a9533fdb442" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@login/%@",_url,userId]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"DELETE"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        /*
                                                        if(data == nil){
                                                            
                                                            NSLog(@"Error at logout : %@", error);
                                                        }else{
                                                            
                                                            check(NO, data, error, response, nil);
                                                        }
                                                         */
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at logout : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at logout : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)linkedInSignUpWithEmail:(NSString*)email
                     firstName:(NSString*)firstName
                linkedInUserId:(NSString*)linkedInUserId
                      lastName:(NSString*)lastName
                activationCode:(NSString*)activationCode
                    isComplete:(jsonResult)check{

    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"1f67b30c-46ed-2492-f34f-ef48b776e7b7" };
    
    NSDictionary *parameters = @{ @"emailAddress": email,
                                  @"firstName": firstName,
                                  @"id": linkedInUserId,
                                  @"lastName": lastName,
                                  @"activation_code": activationCode };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:[NSString stringWithFormat:@"%@linkedin?mode=1",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at linkedIn sign up : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at logout : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)linkedInLoginWithEmail:(NSString*)email
               linkedInUserId:(NSString*)linkedInUserId
                   isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"03671dbf-2bbb-da54-8c72-1d8099f9a378" };
    NSDictionary *parameters = @{ @"emailAddress": email,
                                  @"id": linkedInUserId };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:[NSString stringWithFormat:@"%@linkedin?mode=2",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at linkedIn login : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at logout : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getAllEventsWithToken:(NSString*)token
                  isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"bc072054-a511-4298-27e5-a399ad15486f" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@events",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];

    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at all events : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at all events : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getUpcomingEventsWithToken:(NSString*)token
                       isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"5b777855-358f-a9e6-0652-59b124738ed7" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@events?tag=1",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at upcoming events : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at upcoming events : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getPastEventsWithToken:(NSString*)token
                   isComplete:(jsonResult)check{
    
    NSDictionary *headers = @{ @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"df2236ed-adee-60f5-37b4-460fc2baaa7d" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@events?tag=2",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at past events : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at past events : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getEventWithToken:(NSString*)token
                 eventId:(NSString*)eventId
              isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"02eb35a3-090b-b43d-006d-87f957613773" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@events/%@",_url,eventId]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at event : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at event : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getProfileWithToken:(NSString*)token
                isComplete:(jsonResult)check{

    
    NSDictionary *headers = @{ @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"730a5dae-f2c5-cdbb-0063-1d1a77d89a55" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@profile",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at profile : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at profile : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)changePaswordWithToken:(NSString*)token
              currentPassword:(NSString*)currentPassword
                  newPassword:(NSString*)newPassword
                   isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"f81d982d-ecce-09c2-e5f5-52aee10c266b" };
    
    NSDictionary *parameters = @{ @"data": @{ @"current_password":  currentPassword,
                                              @"password":          newPassword } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@change_password",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at change password : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at change password : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)forgotPasswordWithEmail:(NSString*)email
                    isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"86191477-7b08-b5c9-6917-399bc4d216d8" };
    NSDictionary *parameters = @{ @"data": @{ @"email": email } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@forgot_password",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at forgot password : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at forgot password : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)updateSalutationWithToken:(NSString*)token
                      salutation:(NSString*)salutation
                         loginId:(NSString*)loginId
                      isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"9a505a09-b71f-8b04-3b86-e80e07ccccdd" };
    
    NSDictionary *parameters = @{ @"data": @{ @"salutation": salutation } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@login/%@",_url, loginId]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at update salutation : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at update salutation : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)rsvpWithToken:(NSString*)token
             eventId:(NSString*)eventId
       invitedGuests:(NSArray*)invitedGuests
          isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"08beee0e-a18a-9eea-db44-a568eda2248e" };
    
    //Example adding guests of array
    //NSMutableArray *invitedGuests = [[NSMutableArray alloc] init];
    //for (int i = 0; i < 3; i++) {
    //    NSDictionary *guest = @{ @"name" : @"name 1", @"salutation" : @"Mr" };
    //    [invitedGuests addObject:guest];
    //}
    
    NSDictionary *parameters = @{ @"data": @{ @"event_id":  eventId,
                                              @"guests" :   invitedGuests} };
    
    NSLog(@"parameters : %@",parameters);
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@rsvp",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at rsvp : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at rsvp : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)inviteGuestsWithToken:(NSString*)token
                     eventId:(NSString*)eventId
               invitedGuests:(NSArray*)invitedGuests
                  isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"content-type":  @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"4fbd346e-bf35-c266-abac-e0d3e5873fa2" };

    NSDictionary *parameters = @{ @"data": @{ @"event_id":  eventId,
                                              @"guests" :   invitedGuests} };
    
    NSLog(@"parameters : %@",parameters);
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:[NSString stringWithFormat:@"%@guests",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at invite guest : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at guests : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getViewInvitationsWithToken:(NSString*)token
                           eventId:(NSString*)eventId
                        isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"cache-control": @"no-cache",
                               @"postman-token": @"14704263-b292-6558-a504-046555f649ed" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@guests/%@",_url,eventId]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at view invitations : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at view invitations : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getIcebreakerWithToken:(NSString*)token
                   isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"3f03c545-00a3-35be-668a-504307f2ab5c" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@icebreaker",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at ice breaker : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at ice breaker : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getRedeemWithToken:(NSString*)token
               isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"authorization": token,
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"9679c764-b43a-bfee-9c17-903599f1e734" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:[NSString stringWithFormat:@"%@redeem",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at redeemed token : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at redeem : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}
@end
