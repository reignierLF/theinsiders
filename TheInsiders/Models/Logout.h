//
//  Logout.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 18/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Logout : NSObject

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
