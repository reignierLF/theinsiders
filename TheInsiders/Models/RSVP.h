//
//  RSVP.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSVP : NSObject

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) BOOL statusGuests;
@property (nonatomic, readonly) NSString *messageRsvp;
@property (nonatomic, readonly) NSString *messageGuests;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
