//
//  Details.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 6/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "Details.h"
#import "ImageLoader.h"

@interface Details ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@end

@implementation Details

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    float qrCodeSize = _viewHeight / 3;
    
    ImageLoader *il = [[ImageLoader alloc] init];

    UIImageView *qrCodeImageView = [[UIImageView alloc] initWithFrame:CGRectMake((_viewWidth / 2) - (qrCodeSize / 2), 50, qrCodeSize, qrCodeSize)];
    //qrCodeImageView.image = [UIImage imageNamed:@"qrcode"];
    qrCodeImageView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    qrCodeImageView.clipsToBounds = YES;
    qrCodeImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:qrCodeImageView];
    
    [il parseImage:qrCodeImageView url:_qrCodeImageUrl errorImageName:@"qrcode" style:UIActivityIndicatorViewStyleWhite];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, qrCodeImageView.frame.origin.y + qrCodeImageView.frame.size.height + 10, _viewWidth - 40, 80)];
    //headerLabel.backgroundColor = [UIColor redColor];
    headerLabel.text = @"You are\ncordially invited";
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.numberOfLines = 0;
    headerLabel.lineBreakMode = NSLineBreakByWordWrapping;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"Futura ICG" size:26];
    [self addSubview:headerLabel];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] init];
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"to join "
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:_fromPerson
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),                                                                                          NSBackgroundColorAttributeName: [UIColor clearColor]}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" at "
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:_nameOfEvent
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),                                                                                          NSBackgroundColorAttributeName: [UIColor clearColor]}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" on "
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:_dateOfEvent
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),                                                                                          NSBackgroundColorAttributeName: [UIColor clearColor]}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" at "
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:_placeOfEvent
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),                                                                                          NSBackgroundColorAttributeName: [UIColor clearColor]}]];
    
    UILabel *eventDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, headerLabel.frame.origin.y + headerLabel.frame.size.height, _viewWidth - 20, 60)];
    //eventDetailLabel.backgroundColor = [UIColor redColor];
    eventDetailLabel.numberOfLines = 0;
    eventDetailLabel.attributedText = attributedString;
    eventDetailLabel.textAlignment = NSTextAlignmentCenter;
    //eventDetailLabel.lineBreakMode = NSLineBreakByWordWrapping;
    eventDetailLabel.textColor = [UIColor whiteColor];
    eventDetailLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [eventDetailLabel sizeToFit];
    [self addSubview:eventDetailLabel];
    
    UILabel *instructionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, eventDetailLabel.frame.origin.y + eventDetailLabel.frame.size.height + 10, _viewWidth - 10, 50)];
    //instructionLabel.backgroundColor = [UIColor redColor];
    instructionLabel.text = @"Present this QR Code at the registration\nbooth to recieve priority entry.";
    instructionLabel.textAlignment = NSTextAlignmentCenter;
    instructionLabel.numberOfLines = 0;
    instructionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    instructionLabel.textColor = [UIColor whiteColor];
    instructionLabel.font = [UIFont fontWithName:@"Futura ICG" size:10];
    [self addSubview:instructionLabel];
    
    float offsetToCenter = instructionLabel.frame.origin.y + instructionLabel.frame.size.height;
    
    qrCodeImageView.frame = CGRectMake(qrCodeImageView.frame.origin.x, ((_viewHeight / 2) - (offsetToCenter / 2)) + 40, qrCodeImageView.frame.size.width, qrCodeImageView.frame.size.height);
    
    headerLabel.frame = CGRectMake(headerLabel.frame.origin.x, qrCodeImageView.frame.origin.y + qrCodeImageView.frame.size.height + 10, headerLabel.frame.size.width, headerLabel.frame.size.height);
    
    eventDetailLabel.frame = CGRectMake(eventDetailLabel.frame.origin.x, headerLabel.frame.origin.y + headerLabel.frame.size.height, eventDetailLabel.frame.size.width, eventDetailLabel.frame.size.height);
    
    instructionLabel.frame = CGRectMake(instructionLabel.frame.origin.x, eventDetailLabel.frame.origin.y + eventDetailLabel.frame.size.height + 10, instructionLabel.frame.size.width, instructionLabel.frame.size.height);
}
@end
