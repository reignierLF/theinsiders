//
//  LoginView.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 15/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginView;

@protocol LoginDelegate

-(void)loginEvent:(LoginView*)loginView email:(NSString*)email password:(NSString*)password;
-(void)loginLinkedInEvent:(LoginView*)loginView;
-(void)forgotPasswordEvent:(LoginView*)loginView;

-(void)loginKeyboardWillShow:(NSNotification*)notification;
-(void)loginKeyboardWillBeHidden:(NSNotification*)notification;

@end

@interface LoginView : UIView

@property (nonatomic, weak) id <LoginDelegate> delegate;

-(void)dismissAll;

@end
