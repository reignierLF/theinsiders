//
//  Card.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 20/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Card.h"
#import "ImageLoader.h"

@interface Card ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@end

@implementation Card

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
    }
    
    return self;
}


-(void)didMoveToSuperview{
    
    ImageLoader *il = [[ImageLoader alloc] init];
    
    UIImageView *eventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
    //eventImageView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.1];
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    //eventImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Img0%li",(long)self.tag + 1]];
    eventImageView.clipsToBounds = YES;
    eventImageView.layer.cornerRadius = self.layer.cornerRadius;
    [eventImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self addSubview:eventImageView];
    
    [il parseImage:eventImageView url:_imageUrl errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
    
    _detailView = [[UIView alloc] initWithFrame:CGRectMake(0, _viewHeight - 80, _viewWidth, 80)];
    _detailView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1.0];
    [self addSubview:_detailView];
    
    UIBezierPath *cornerMaskPath = [UIBezierPath bezierPathWithRoundedRect:_detailView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(self.layer.cornerRadius, self.layer.cornerRadius)];
    
    CAShapeLayer *cornerMaskLayer = [[CAShapeLayer alloc] init];
    cornerMaskLayer.frame = _detailView.bounds;
    cornerMaskLayer.path  = cornerMaskPath.CGPath;
    _detailView.layer.mask = cornerMaskLayer;
    
    UIImageView *locationImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"placeholder"]];
    locationImageView.frame = CGRectMake(20, (_detailView.frame.size.height / 2) - (20 + 5), 20, 20);
    //locationImageView.backgroundColor = [UIColor orangeColor];
    locationImageView.contentMode = UIViewContentModeScaleAspectFit;
    locationImageView.clipsToBounds = YES;
    [locationImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_detailView addSubview:locationImageView];
    
    UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(locationImageView.frame.origin.x + locationImageView.frame.size.width + 5, locationImageView.frame.origin.y, _detailView.frame.size.width - ((locationImageView.frame.origin.x * 2) + locationImageView.frame.size.width), locationImageView.frame.size.height)];
    //locationLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //locationLabel.text = @"Club Answer, Seoul, South Korea";
    locationLabel.text = _location;
    locationLabel.font = [UIFont fontWithName:@"Futura ICG" size:10];
    [locationLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_detailView addSubview:locationLabel];
    
    UIImageView *calendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendarSmall"]];
    calendarImageView.frame = CGRectMake(20, (_detailView.frame.size.height  / 2) + 5, 20, 20);
    //calendarImageView.backgroundColor = [UIColor orangeColor];
    calendarImageView.contentMode = UIViewContentModeScaleAspectFit;
    calendarImageView.clipsToBounds = YES;
    [calendarImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_detailView addSubview:calendarImageView];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(calendarImageView.frame.origin.x + calendarImageView.frame.size.width + 5, calendarImageView.frame.origin.y, locationLabel.frame.size.width, calendarImageView.frame.size.height)];
    //dateLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //dateLabel.text = @"20 January 2017, 8:00 PM";
    dateLabel.text = _date;
    dateLabel.font = [UIFont fontWithName:@"Futura ICG" size:10];
    [dateLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_detailView addSubview:dateLabel];
    
    UILabel *cardNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 40, 20)];
    //cardNumberLabel.backgroundColor = [UIColor redColor];
    cardNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)self.tag];
    cardNumberLabel.textAlignment = NSTextAlignmentCenter;
    cardNumberLabel.textColor = [UIColor whiteColor];
    cardNumberLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [self addSubview:cardNumberLabel];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    //button.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    button.frame = CGRectMake(0, 0, _viewWidth, _viewHeight);
    button.tag = self.tag;
    [button setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self addSubview:button];
    
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)buttonPressed:(UIButton*)sender{

    [_delegate didClickCard:self sender:sender];
}

@end
