//
//  SwitchView.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 15/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "SwitchView.h"

@interface SwitchView ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation SwitchView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
    }
    
    return self;
}

-(void)didMoveToSuperview{

    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
    //_scrollView.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.4];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * 2, _scrollView.frame.size.height);
    _scrollView.delaysContentTouches = NO;
    _scrollView.scrollEnabled = NO;
    [self addSubview:_scrollView];
    
    [_scrollView scrollRectToVisible:CGRectMake(_viewWidth, 0, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:YES];
    
    _firstView.frame = CGRectMake(0, 0, _firstView.frame.size.width, _viewHeight);
    [_scrollView addSubview:_firstView];
    
    _secondView.frame = CGRectMake(_scrollView.frame.size.width, 0, _secondView.frame.size.width, _viewHeight);
    [_scrollView addSubview:_secondView];
}

-(void)switchViewWithIndex:(NSInteger)index{
    
    float width = CGRectGetWidth(_scrollView.frame);
    float height = CGRectGetHeight(_scrollView.frame);
    float newPosition = width * index;
    CGRect toVisible = CGRectMake(newPosition, 0, width, height);
    
    [_scrollView scrollRectToVisible:toVisible animated:YES];
    
    NSLog(@"visible to %f", toVisible.origin.x);
}
@end
