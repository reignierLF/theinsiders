//
//  SignUpView.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 15/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SignUpView;

@protocol SignUpDelegate

-(void)getStartedEvent:(SignUpView*)signUpView
                 email:(NSString*)email
              password:(NSString*)password
             firstName:(NSString*)fName
              lastName:(NSString*)lName
                   dob:(NSString*)dob
              mobileNo:(NSString*)mobileNo;

-(void)signUpKeyboardWillShow:(NSNotification*)notification;
-(void)signUpKeyboardWillBeHidden:(NSNotification*)notification;

@end

@interface SignUpView : UIView

@property (nonatomic, weak) id <SignUpDelegate> delegate;

-(void)dismissAll;

@end
