//
//  ActivationView.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 12/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "ActivationView.h"

#import "cUITextField.h"

@interface ActivationView ()<UITextFieldDelegate>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) cUITextField *activationCode;
@property (nonatomic, strong) UIButton *activate;
@property (nonatomic, strong) UIButton *linkedInSignUp;

@end

@implementation ActivationView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
        
        //self.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    _activationCode = [[cUITextField alloc] initWithFrame:CGRectMake(20, 10, _viewWidth - 40, 30)];
    _activationCode.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    //_activationCode.text = @"Activation Code";
    //_activationCode.placeholder = @"Activation Code";
    _activationCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Activation Code" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _activationCode.textColor = [UIColor whiteColor];
    _activationCode.layer.cornerRadius = 3;
    _activationCode.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _activationCode.delegate = self;
    [self addSubview:_activationCode];
    
    _activate = [UIButton buttonWithType:UIButtonTypeSystem];
    _activate.frame = CGRectMake(_activationCode.frame.origin.x, _activationCode.frame.origin.y + _activationCode.frame.size.height + 6, _activationCode.frame.size.width, 50);
    _activate.layer.cornerRadius = 3;
    _activate.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    _activate.layer.borderWidth = 2;
    _activate.tag = 0;
    _activate.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_activate setTitle:@"ACTIVATE" forState:UIControlStateNormal];
    [_activate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:_activate];
    
    [_activate addTarget:self action:@selector(activateEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *dividerLabel = [[UILabel alloc] initWithFrame:CGRectMake(_activate.frame.origin.x, _activate.frame.origin.y + _activate.frame.size.height + 5, _activate.frame.size.width, _activate.frame.size.height)];
    dividerLabel.text = @"OR";
    dividerLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    dividerLabel.textAlignment = NSTextAlignmentCenter;
    dividerLabel.textColor = [UIColor whiteColor];
    [self addSubview:dividerLabel];
    
    _linkedInSignUp = [UIButton buttonWithType:UIButtonTypeSystem];
    _linkedInSignUp.frame = CGRectMake(dividerLabel.frame.origin.x, dividerLabel.frame.origin.y + dividerLabel.frame.size.height + 5, _activate.frame.size.width, _activate.frame.size.height);
    _linkedInSignUp.layer.cornerRadius = 3;
    _linkedInSignUp.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    _linkedInSignUp.layer.borderWidth = 2;
    _linkedInSignUp.tag = 1;
    _linkedInSignUp.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_linkedInSignUp setTitle:@"Sign Up With LinkedIn" forState:UIControlStateNormal];
    [_linkedInSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:_linkedInSignUp];
    
    [_linkedInSignUp addTarget:self action:@selector(activateEvent:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)dismissAll{

    _activationCode.text = @"";
    [_activationCode resignFirstResponder];
}

-(void)activateEvent:(UIButton*)sender{
    
    [_activationCode resignFirstResponder];
    
    if(sender.tag == 0){
        
        [_delegate activate:self code:_activationCode.text];
    }else if(sender.tag == 1){
        
        [_delegate activateWithLinkedIn:self code:_activationCode.text];
    }
}

/*
 * ==================== Delegates ====================
 */

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Activation Code" attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:0.25]}];
    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Activation Code" attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:1.0]}];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
