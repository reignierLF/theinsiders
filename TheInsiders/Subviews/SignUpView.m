//
//  SignUpView.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 15/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "SignUpView.h"

#import "cUITextField.h"

@interface SignUpView () <UITextFieldDelegate>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) cUITextField *firstName;
@property (nonatomic, strong) cUITextField *lastName;
@property (nonatomic, strong) cUITextField *emailAddress;
@property (nonatomic, strong) cUITextField *password;
@property (nonatomic, strong) cUITextField *mobileNumber;
@property (nonatomic, strong) cUITextField *birthday;

@property (nonatomic, strong) UIButton *getStarted;

@end

@implementation SignUpView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
        
        [self initTextFields];
        [self initButtons];
    }
    
    return self;
}

-(void)didMoveToSuperview{

    
}

-(void)initTextFields{
    
    _firstName = [[cUITextField alloc] initWithFrame:CGRectMake(20, 10, ((_viewWidth / 2) - 23), 30)];
    _firstName.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    _firstName.delegate = self;
    //_firstName.text = @"First Name";
    //_firstName.placeholder = @"First Name";
    _firstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _firstName.textColor = [UIColor whiteColor];
    _firstName.layer.cornerRadius = 3;
    _firstName.font = [UIFont fontWithName:@"Futura ICG" size:13];
    [self addSubview:_firstName];
    
    _lastName = [[cUITextField alloc] initWithFrame:CGRectMake((_viewWidth / 2) + 3, 10, ((_viewWidth / 2) - 23), _firstName.frame.size.height)];
    _lastName.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    _lastName.delegate = self;
    //_lastName.text = @"Last Name";
    //_lastName.placeholder = @"Last Name";
    _lastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _lastName.textColor = [UIColor whiteColor];
    _lastName.layer.cornerRadius = 3;
    _lastName.font = [UIFont fontWithName:@"Futura ICG" size:13];
    [self addSubview:_lastName];
    
    _emailAddress = [[cUITextField alloc] initWithFrame:CGRectMake(_firstName.frame.origin.x, _lastName.frame.origin.y + _lastName.frame.size.height + 6, _viewWidth - 40, _lastName.frame.size.height)];
    _emailAddress.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    _emailAddress.delegate = self;
    //_emailAddress.text = @"Email Address";
    //_emailAddress.placeholder = @"Email Address";
    _emailAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _emailAddress.textColor = [UIColor whiteColor];
    _emailAddress.layer.cornerRadius = 3;
    _emailAddress.font = [UIFont fontWithName:@"Futura ICG" size:13];
    [self addSubview:_emailAddress];
    
    _password = [[cUITextField alloc] initWithFrame:CGRectMake(_emailAddress.frame.origin.x, _emailAddress.frame.origin.y + _emailAddress.frame.size.height + 6, _emailAddress.frame.size.width, _emailAddress.frame.size.height)];
    _password.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    _password.delegate = self;
    //_password.text = @"Password";
    //_password.placeholder = @"Password";
    _password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _password.textColor = [UIColor whiteColor];
    _password.layer.cornerRadius = 3;
    _password.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _password.secureTextEntry = YES;
    [self addSubview:_password];
    
    _mobileNumber = [[cUITextField alloc] initWithFrame:CGRectMake(_password.frame.origin.x, _password.frame.origin.y + _password.frame.size.height + 6, _password.frame.size.width, _password.frame.size.height)];
    _mobileNumber.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    _mobileNumber.delegate = self;
    //_mobileNumber.text = @"Mobile Number";
    //_mobileNumber.placeholder = @"Mobile Number";
    _mobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _mobileNumber.textColor = [UIColor whiteColor];
    _mobileNumber.layer.cornerRadius = 3;
    _mobileNumber.font = [UIFont fontWithName:@"Futura ICG" size:13];
    [self addSubview:_mobileNumber];
    
    _birthday = [[cUITextField alloc] initWithFrame:CGRectMake(_mobileNumber.frame.origin.x, _mobileNumber.frame.origin.y + _mobileNumber.frame.size.height + 6, _mobileNumber.frame.size.width, _mobileNumber.frame.size.height)];
    _birthday.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    _birthday.delegate = self;
    //_birthday.text = @"Birthday";
    //_birthday.placeholder = @"Birthday";
    _birthday.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Birthday" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _birthday.textColor = [UIColor whiteColor];
    _birthday.layer.cornerRadius = 3;
    _birthday.font = [UIFont fontWithName:@"Futura ICG" size:13];
    [self addSubview:_birthday];
    
    UIDatePicker *picker1   = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, _viewHeight - (_viewHeight / 2), _viewWidth, _viewHeight / 2)];
    [picker1 setDatePickerMode:UIDatePickerModeDate];
    picker1.backgroundColor = [UIColor whiteColor];
    [picker1 addTarget:self action:@selector(startDateSelected:) forControlEvents:UIControlEventValueChanged];
    
    //    [picker1 addSubview:toolBar];
    _birthday.inputView  = picker1; // Here birthTxt is Textfield Name replace with your name
    
    [_birthday addTarget : self
               action : @selector(dismissDate:)
     forControlEvents : UIControlEventTouchUpOutside];
    
    // single tap gesture recognizer
    UITapGestureRecognizer *tapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDate:)];
    tapGestureRecognize.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tapGestureRecognize];
}

-(void)initButtons{
    
    _getStarted = [UIButton buttonWithType:UIButtonTypeSystem];
    _getStarted.frame = CGRectMake(_birthday.frame.origin.x, _birthday.frame.origin.y + _birthday.frame.size.height + 6, _birthday.frame.size.width, 50);
    _getStarted.layer.cornerRadius = 3;
    _getStarted.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    _getStarted.layer.borderWidth = 2;
    _getStarted.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_getStarted setTitle:@"GET STARTED" forState:UIControlStateNormal];
    [_getStarted setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:_getStarted];
    
    [_getStarted addTarget:self action:@selector(buttonEvents:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillShow:)
                                                 name: UIKeyboardWillChangeFrameNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillBeHidden:)
                                                 name: UIKeyboardWillHideNotification
                                               object: nil];
}

- (void)startDateSelected:(UIDatePicker *)datePicker{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    dateFormat.dateStyle = NSDateFormatterMediumStyle;
    NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
    
    _birthday.text = dateString;
}

- (void)dismissDate:(UIGestureRecognizer *)gestureRecognizer {
    
    [_birthday resignFirstResponder];
    
    NSLog(@"Resign your responder");
}

-(void)dismissAll{
    
    _emailAddress.text = @"";
    _password.text = @"";
    _firstName.text = @"";
    _lastName.text = @"";
    _birthday.text = @"";
    _mobileNumber.text = @"";

    [_firstName resignFirstResponder];
    [_lastName resignFirstResponder];
    [_emailAddress resignFirstResponder];
    [_password resignFirstResponder];
    [_mobileNumber resignFirstResponder];
    [_birthday resignFirstResponder];
}

-(void)buttonEvents:(UIButton*)sender{
    
    [_firstName resignFirstResponder];
    [_lastName resignFirstResponder];
    [_emailAddress resignFirstResponder];
    [_password resignFirstResponder];
    [_mobileNumber resignFirstResponder];
    [_birthday resignFirstResponder];
    
    [_delegate getStartedEvent:self email:_emailAddress.text password:_password.text firstName:_firstName.text lastName:_lastName.text dob:_birthday.text mobileNo:_mobileNumber.text];
    
    //_emailAddress.text = @"";
    //_password.text = @"";
    //_firstName.text = @"";
    //_lastName.text = @"";
    //_birthday.text = @"";
    //_mobileNumber.text = @"";
}

/*
 * ==================== Delegates ====================
 */

-(void)keyboardWillShow:(NSNotification *)notification {
    
    [_delegate signUpKeyboardWillShow:notification];
}

-(void)keyboardWillBeHidden:(NSNotification *)notification{
    
    [_delegate signUpKeyboardWillBeHidden:notification];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //textField.text = @"";
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:0.25]}];
    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:1.0]}];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
    
    return YES;
}
@end
