//
//  TimerView.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 6/2/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TimerView;

@protocol TimerDelegate

-(void)didFinishCountingDown:(TimerView*)timerView;

@end

@interface TimerView : UIView

@property (nonatomic, weak) id <TimerDelegate> delegate;

@end
