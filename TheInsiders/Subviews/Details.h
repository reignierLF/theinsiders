//
//  Details.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 6/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Details : UIView

@property (nonatomic, strong) NSString *qrCodeImageUrl;
@property (nonatomic, strong) NSString *fromPerson;
@property (nonatomic, strong) NSString *nameOfEvent;
@property (nonatomic, strong) NSString *dateOfEvent;
@property (nonatomic, strong) NSString *placeOfEvent;

@end
