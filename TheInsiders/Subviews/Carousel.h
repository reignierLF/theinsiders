//
//  Carousel.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Carousel;

@protocol CarouselDelegate

-(void)didChooseEventCard:(Carousel*)carousel sender:(UIButton*)sender;

@end

@interface Carousel : UIView

@property (nonatomic) float threshold;
@property (nonatomic) float shadowIntensity;

@property (nonatomic) NSInteger size;

@property (nonatomic, strong) NSArray *allAddress;
@property (nonatomic, strong) NSArray *allEventImageUrl;
@property (nonatomic, strong) NSArray *allEventId;
@property (nonatomic, strong) NSArray *allSponsorLogoUrl;
@property (nonatomic, strong) NSArray *allDateTime;

@property (nonatomic, weak) id <CarouselDelegate> delegate;

@end
