//
//  Carousel.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Carousel;

@protocol CarouselDelegate

-(void)didChooseEventCard:(Carousel*)carousel sender:(UIButton*)sender;

@end

@interface Carousel : UIView

@property (nonatomic) float threshold;
@property (nonatomic) float margin;
@property (nonatomic) float minSize;

@property (nonatomic, weak) id <CarouselDelegate> delegate;

@end
