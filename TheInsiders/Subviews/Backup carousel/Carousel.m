//
//  Carousel.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Carousel.h"
#import "cUIScrollView.h"
#import "Card.h"

@interface Carousel () <UIScrollViewDelegate, CardDelegate>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;
@property (nonatomic) NSInteger carouselSize;
@property (nonatomic) NSInteger carouselCounter;

@property (nonatomic, strong) UIScrollView *carouselScrollView;

@property (nonatomic, strong) NSMutableArray *cardsArray;

@property (nonatomic, strong) Card *previousCard;
@property (nonatomic, strong) Card *currentCard;
@property (nonatomic, strong) Card *nextCard;

@property (nonatomic) NSInteger previousIndex;
@property (nonatomic) NSInteger currentIndex;
@property (nonatomic) NSInteger nextIndex;

@property (nonatomic) float zoomFactor;

@property (nonatomic) float originalXPos;
@property (nonatomic) float originalYPos;
@property (nonatomic) float originalWidth;
@property (nonatomic) float originalHeight;

@end

@implementation Carousel

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
        
        _threshold = 50;
        
        _cardsArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

-(void)didMoveToSuperview{

    [self initCarousel];
}

-(void)initCarousel{
    
    _carouselSize = 5;
    
    _originalWidth = _viewWidth - (_viewWidth / 3);
    _originalHeight = _viewHeight - (_viewHeight / 8);
    
    _carouselScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(((_viewWidth / 2) - (_originalWidth / 2)) - 10, 0, _originalWidth + 20, _originalHeight)];
    //_carouselScrollView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
    _carouselScrollView.delegate = self;
    _carouselScrollView.bounces = NO;
    _carouselScrollView.pagingEnabled = YES;
    _carouselScrollView.contentSize = CGSizeMake(_carouselScrollView.frame.size.width  * (_carouselSize + 2), _carouselScrollView.frame.size.height);
    _carouselScrollView.showsHorizontalScrollIndicator = YES;
    _carouselScrollView.clipsToBounds = NO;
    [self addSubview:_carouselScrollView];
    
    [_carouselScrollView scrollRectToVisible:CGRectMake(_carouselScrollView.frame.size.width, _carouselScrollView.frame.origin.y, _carouselScrollView.frame.size.width, _carouselScrollView.frame.size.height) animated:NO];
    
    NSInteger infiniteLoop = 0;
    
    _carouselCounter = infiniteLoop;
    
    for (int i = 0; i < _carouselSize + 2; i++) {
        
        if(i == _carouselSize + 1){
            
            infiniteLoop = 0;
            
            NSLog(@"last object = %ld", (long)infiniteLoop);
        }else if(i == 0){
            
            infiniteLoop = _carouselSize - 1;
            
            if(infiniteLoop < 0){
                
                infiniteLoop = 0;
            }
            
            NSLog(@"1st object = %ld", (long)infiniteLoop);
        }else{
            
            infiniteLoop = i - 1;
        }
        
        float cardHeight = 0;
        float cardYPos = 0;
        
        if(i != 1){
            
            cardHeight = _originalHeight - _threshold;
            cardYPos = _threshold / 2;
        }else{
        
            cardHeight = _originalHeight;
            cardYPos = 0;
        }
        
        float percent = fabs((100 * _threshold) / _originalHeight);
        
        Card *card = [[Card alloc] initWithFrame:CGRectMake(((_originalWidth + 10) * i) + ((i * 10) + 10), cardYPos, _originalWidth , cardHeight)];
        card.delegate = self;
        card.autoresizesSubviews = YES;
        card.backgroundColor = [UIColor orangeColor];
        card.tag = infiniteLoop;
        card.layer.cornerRadius = 5;
        [_carouselScrollView addSubview:card];
        
        [_cardsArray addObject:card];
        
        //NSLog(@"all tags : %li",(long)_carouselCounter);
        //_ccHeight - fabs((offsetPercentage / 100) * _threshold)
        
        NSLog(@"%f %%",percent);
        
        NSLog(@"%f", _originalWidth - _originalWidth * (percent / 100));
    }
    
    NSLog(@"all cards : %lu", (unsigned long)_cardsArray.count);
    
    _previousIndex = _carouselCounter;
    _currentIndex = _carouselCounter + 1;
    _nextIndex = _carouselCounter + 2;
    
    NSLog(@"previous > onStart: %ld",(long)_previousCard.tag);
    NSLog(@"current > onStart: %ld",(long)_currentCard.tag);
    NSLog(@"next > onStart: %ld", (long)_nextCard.tag);
}

#pragma mark Events


#pragma mark Delegates

-(void)didClickCard:(Card*)card sender:(UIButton*)sender{
    
    [_delegate didChooseEventCard:self sender:sender];
}

-(void)scrollViewDidEndDecelerating:(cUIScrollView *)scrollView{
    
    _carouselCounter = (int)(scrollView.contentOffset.x) / (int)(scrollView.frame.size.width);
    
    NSLog(@"_carouselCounter = %ld",(long)_carouselCounter);
    
    if((int)(_carouselScrollView.contentOffset.x) >= (int)(_carouselScrollView.contentSize.width - _carouselScrollView.frame.size.width)){
        
        NSLog(@"back to start");
        
        _previousIndex = 0;
        _currentIndex = 1;
        _nextIndex = 2;
        
        [scrollView scrollRectToVisible:CGRectMake(scrollView.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];
        
        [self resizeCard];
    }else if(scrollView.contentOffset.x == 0){
        
        NSLog(@"go to end");
        
        _previousIndex = _cardsArray.count - 3;
        _currentIndex = _cardsArray.count - 2;
        _nextIndex = _cardsArray.count - 1;
        
        [scrollView scrollRectToVisible:CGRectMake(scrollView.frame.size.width * _carouselSize, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];

        [self resizeCard];
    }else{
        
        if((_carouselCounter - 1) < 0){
            
            _previousIndex = _cardsArray.count - 3;
        }else{
        
            _previousIndex = _carouselCounter - 1;
        }
        
        _currentIndex = _carouselCounter;
        
        if((_carouselCounter + 1) == _cardsArray.count){
            
            _nextIndex = 1;
        }else{
        
            _nextIndex = _carouselCounter + 1;
        }
        
        NSLog(@"normal scrolling");
    }
    
    NSLog(@"previous > end: %ld",(long)_previousIndex);
    NSLog(@"current > end: %ld",(long)_currentIndex);
    NSLog(@"next > end: %ld", (long)_nextIndex);

    _zoomFactor = 0;
}

-(void)scrollViewDidScroll:(cUIScrollView *)scrollView{
    
    //NSLog(@"previous > scroll: %ld",(long)_previousCard.tag);
    //NSLog(@"current > scroll: %ld",(long)_currentCard.tag);
    //NSLog(@"next > scroll: %ld", (long)_nextCard.tag);
    
    //NSLog(@"previous > scroll: %ld",(long)_previousIndex);
    //NSLog(@"current > scroll: %ld | tag : %li",(long)_currentIndex,(long)_currentCard.tag);
    //NSLog(@"next > scroll: %ld", (long)_nextIndex);
    
    _zoomFactor = scrollView.contentOffset.x - (scrollView.frame.size.width * _currentIndex);
    
    float offsetPercentage = fabs((100 * _zoomFactor) / scrollView.frame.size.width);
    
    for (int i = 0; i < _cardsArray.count; i++) {
        
        Card *card = [_cardsArray objectAtIndex:i];
        
        card.frame = CGRectMake(card.frame.origin.x, _threshold / 2, _originalWidth, _originalHeight - _threshold);
            
        if(i == _previousIndex){
            
            card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y - fabs((offsetPercentage / 100) * (_threshold / 2)), card.frame.size.width, card.frame.size.height + fabs((offsetPercentage / 100) * _threshold));
        }
        
        if(i == _currentIndex){
            
            card.frame = CGRectMake(card.frame.origin.x, fabs((offsetPercentage / 100) * (_threshold / 2)), card.frame.size.width, _originalHeight - fabs((offsetPercentage / 100) * _threshold));
        }
        
        if(i == _nextIndex){
            card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y - fabs((offsetPercentage / 100) * (_threshold / 2)), card.frame.size.width, card.frame.size.height + fabs((offsetPercentage / 100) * _threshold));
        }
    }
}

-(void)resizeCard{
    
    NSLog(@"previous > resizeCard: %ld",(long)_previousIndex);
    NSLog(@"current > resizeCard: %ld",(long)_currentIndex);
    NSLog(@"next > resizeCard: %ld", (long)_nextIndex);

    for (int i = 0; i < _cardsArray.count; i++) {
        
        Card *card = [_cardsArray objectAtIndex:i];
        
        card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, _originalWidth, _originalHeight - _threshold);

        if(i == _currentIndex){
            
            card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, _originalHeight);
        }
    }
    
    NSLog(@"resize all cards");
}
@end
