//
//  EventsCell.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 9/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "EventsCell.h"
#import "ImageLoader.h"

@interface EventsCell ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@end

@implementation EventsCell

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
        
        _hasInvited = NO;
        _isPastEvent = NO;
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    ImageLoader *il = [[ImageLoader alloc] init];

    UIImageView *eventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
    eventImageView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    //eventImageView.image = [UIImage imageNamed:@"myEventsParty"];
    eventImageView.clipsToBounds = YES;
    [self addSubview:eventImageView];
    
    [il parseImage:eventImageView url:_eventImageUrl errorImageName:@"" style:UIActivityIndicatorViewStyleWhite];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _viewHeight - (10 + 15), _viewWidth - 40, 15)];
    //dateLabel.backgroundColor = [UIColor redColor];
    //dateLabel.text = @"20 January 2017, 8:00 PM";
    dateLabel.text = _dateTime;
    dateLabel.textColor = [UIColor whiteColor];
    dateLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    [self addSubview:dateLabel];
    
    UILabel *placeLabel = [[UILabel alloc] initWithFrame:CGRectMake(dateLabel.frame.origin.x, dateLabel.frame.origin.y - dateLabel.frame.size.height, dateLabel.frame.size.width, dateLabel.frame.size.height)];
    //placeLabel.backgroundColor = [UIColor redColor];
    //placeLabel.text = @"Club Answer, Seoul, South Korea";
    placeLabel.text = _address;
    placeLabel.textColor = [UIColor whiteColor];
    placeLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    [self addSubview:placeLabel];
    
    UILabel *eventLabel = [[UILabel alloc] initWithFrame:CGRectMake(placeLabel.frame.origin.x, placeLabel.frame.origin.y - 25, placeLabel.frame.size.width, 25)];
    //eventLabel.backgroundColor = [UIColor redColor];
    //eventLabel.text = @"Color Fuzion";
    eventLabel.text = _title;
    eventLabel.textColor = [UIColor whiteColor];
    eventLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [eventLabel sizeToFit];
    [self addSubview:eventLabel];
    
    NSString *status;
    UIColor *statusColor;
    
    if(_hasInvited){
    
        status = @"INVITED";
        
        statusColor = [[UIColor redColor] colorWithAlphaComponent:0.8];
    }else{
        
        status = @"YET TO INVITE";
        
        statusColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
    }
    
    UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(eventLabel.frame.origin.x + eventLabel.frame.size.width + 5, eventLabel.frame.origin.y, 100, eventLabel.frame.size.height)];
    //statusLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
    statusLabel.text = status;
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.textColor = [UIColor whiteColor];
    statusLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    statusLabel.frame = CGRectMake(statusLabel.frame.origin.x, statusLabel.frame.origin.y, statusLabel.intrinsicContentSize.width + 10, statusLabel.frame.size.height);
    
    if(!_isPastEvent){
        
        UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(statusLabel.frame.origin.x, statusLabel.frame.origin.y, statusLabel.frame.size.width, statusLabel.frame.size.height)];
        statusView.backgroundColor = statusColor;
        statusView.layer.cornerRadius = 4;
        [self addSubview:statusView];
        [self addSubview:statusLabel];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    //button.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    button.tag = self.tag;
    button.frame = CGRectMake(0, 0, _viewWidth, _viewHeight);
    [self addSubview:button];
    
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)buttonPressed:(UIButton*)sender{
    
    [_delegate didClickCell:self sender:sender];
}
@end
