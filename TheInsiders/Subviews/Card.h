//
//  Card.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 20/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Card;

@protocol CardDelegate

-(void)didClickCard:(Card*)card sender:(UIButton*)sender;

@end

@interface Card : UIView

@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) UIView *detailView;

@property (nonatomic, weak) id <CardDelegate> delegate;

@end
