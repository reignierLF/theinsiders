//
//  AdditionalInvite.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 4/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AdditionalInvite;

@protocol AdditionalInviteDelegate

-(void)AdditionalInviteDidPopUp:(AdditionalInvite*)additionalInvite;
-(void)AdditionalKeyboardWillShow:(NSNotification*)notification;
-(void)AdditionalKeyboardWillBeHidden:(NSNotification*)notification;

@end

@interface AdditionalInvite : UIView

@property (nonatomic, strong) UILabel *salutationLabel;
@property (nonatomic, strong) NSString *invitedFullName;

@property (weak, nonatomic) id <AdditionalInviteDelegate> delegate;

@end
