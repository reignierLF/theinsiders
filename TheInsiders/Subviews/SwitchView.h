//
//  SwitchView.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 15/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchView : UIView

@property (nonatomic, strong) UIView *firstView;
@property (nonatomic, strong) UIView *secondView;

-(void)switchViewWithIndex:(NSInteger)index;

@end
