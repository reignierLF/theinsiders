//
//  ListEvents.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 24/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ListEvents;

@protocol ListEventsDelegate

-(void)didSelectEvent:(ListEvents*)eventsCell sender:(UIButton*)sender;

@end

@interface ListEvents : UIView

@property (nonatomic) NSInteger size;
@property (nonatomic, strong) NSArray *address;
@property (nonatomic, strong) NSArray *dateTime;
@property (nonatomic, strong) NSArray *eventImageUrl;
@property (nonatomic, strong) NSArray *hasInvited;
@property (nonatomic, strong) NSArray *eventId;
@property (nonatomic, strong) NSArray *title;
@property (nonatomic) BOOL isPastEvent;

@property (nonatomic, weak) id <ListEventsDelegate> delegate;

@end
