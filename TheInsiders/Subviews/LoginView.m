//
//  LoginView.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 15/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "LoginView.h"

#import "cUITextField.h"

@interface LoginView () <UITextFieldDelegate>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) cUITextField *emailAddress;
@property (nonatomic, strong) cUITextField *password;

@property (nonatomic, strong) UIButton *login;
@property (nonatomic, strong) UIButton *linkedInLogin;

@end

@implementation LoginView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
        
        [self initTextFields];
        [self initButtons];
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    
}

-(void)initTextFields{

    _emailAddress = [[cUITextField alloc] initWithFrame:CGRectMake(20, 10, _viewWidth - 40, 30)];
    _emailAddress.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    //_emailAddress.text = @"Email Address";
    //_emailAddress.placeholder = @"Email Address";
    _emailAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _emailAddress.textColor = [UIColor whiteColor];
    _emailAddress.layer.cornerRadius = 3;
    //_emailAddress.font = [UIFont boldSystemFontOfSize:13];
    _emailAddress.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _emailAddress.delegate = self;
    [self addSubview:_emailAddress];
    
    _password = [[cUITextField alloc] initWithFrame:CGRectMake(_emailAddress.frame.origin.x, _emailAddress.frame.origin.y + _emailAddress.frame.size.height + 6, _emailAddress.frame.size.width, _emailAddress.frame.size.height)];
    _password.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    //_password.text = @"Password";
    //_password.placeholder = @"Password";
    _password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _password.textColor = [UIColor whiteColor];
    _password.layer.cornerRadius = 3;
    //_password.font = [UIFont boldSystemFontOfSize:13];
    _password.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _password.delegate = self;
    _password.secureTextEntry = YES;
    [self addSubview:_password];
}

-(void)initButtons{

    _login = [UIButton buttonWithType:UIButtonTypeSystem];
    _login.frame = CGRectMake(_password.frame.origin.x, _password.frame.origin.y + _password.frame.size.height + 6, _password.frame.size.width, 50);
    _login.layer.cornerRadius = 3;
    _login.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    _login.layer.borderWidth = 2;
    _login.tag = 0;
    _login.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_login setTitle:@"LOG IN" forState:UIControlStateNormal];
    [_login setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:_login];
    
    [_login addTarget:self action:@selector(buttonEvents:) forControlEvents:UIControlEventTouchUpInside];
    
    _linkedInLogin = [UIButton buttonWithType:UIButtonTypeSystem];
    _linkedInLogin.frame = CGRectMake(_login.frame.origin.x, _login.frame.origin.y + _login.frame.size.height + 6, _login.frame.size.width, _login.frame.size.height);
    _linkedInLogin.layer.cornerRadius = 3;
    _linkedInLogin.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    _linkedInLogin.layer.borderWidth = 2;
    _linkedInLogin.tag = 1;
    _linkedInLogin.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_linkedInLogin setTitle:@"Log In With LinkedIn" forState:UIControlStateNormal];
    [_linkedInLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:_linkedInLogin];
    
    [_linkedInLogin addTarget:self action:@selector(buttonEvents:) forControlEvents:UIControlEventTouchUpInside];
    
    [self initDevVersionLayout];
    
    UIButton *forgotPassword = [UIButton buttonWithType:UIButtonTypeSystem];
    //forgotPassword.backgroundColor = [UIColor redColor];
    forgotPassword.frame = CGRectMake(20, _viewHeight - 50, _viewWidth - 40, 40);
    forgotPassword.tag = 2;
    forgotPassword.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [forgotPassword setTitle:@"Forgot Your Password?" forState:UIControlStateNormal];
    [forgotPassword setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:forgotPassword];
    
    [forgotPassword addTarget:self action:@selector(buttonEvents:) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     * Detect keyboard whether will show or hide
     * Used for screen to move up base on the height of keyboard
     * and so it doest overlap the keyboard over the email, password input and login button
     */
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillShow:)
                                                 name: UIKeyboardWillChangeFrameNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillBeHidden:)
                                                 name: UIKeyboardWillHideNotification
                                               object: nil];
}

-(void)initDevVersionLayout{
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];

    UILabel *versionAndBundleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _linkedInLogin.frame.origin.y + _linkedInLogin.frame.size.height + 40, _viewWidth - 40, 50)];
    //userFullNameLabel.backgroundColor = [UIColor redColor];
    versionAndBundleLabel.text = [NSString stringWithFormat:@"Version : %@\nBuidle : %@", version, build];
    versionAndBundleLabel.textAlignment = NSTextAlignmentCenter;
    versionAndBundleLabel.textColor = [UIColor whiteColor];
    versionAndBundleLabel.font = [UIFont fontWithName:@"Futura ICG" size:12];
    versionAndBundleLabel.numberOfLines = 0;
    versionAndBundleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:versionAndBundleLabel];
}

-(void)dismissAll{

    _emailAddress.text = @"";
    _password.text = @"";
    
    [_emailAddress resignFirstResponder];
    [_password resignFirstResponder];
}

-(void)buttonEvents:(UIButton*)sender{
    
    [_emailAddress resignFirstResponder];
    [_password resignFirstResponder];
    
    if(sender.tag == 0){
        
        [_delegate loginEvent:self email:_emailAddress.text password:_password.text];
        
        //_emailAddress.text = @"";
        //_password.text = @"";
        
    }else if(sender.tag == 1){
        
        [_delegate loginLinkedInEvent:self];
    }else if(sender.tag == 2){
    
        [_delegate forgotPasswordEvent:self];
    }
}

/*
 * ==================== Delegates ====================
 */

-(void)keyboardWillShow:(NSNotification *)notification {
    
    [_delegate loginKeyboardWillShow:notification];
}

-(void)keyboardWillBeHidden:(NSNotification *)notification{
    
    [_delegate loginKeyboardWillBeHidden:notification];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    /*
     * When the textfield was click
     * remove any inputs in the text field
     */
    
    //textField.text = @"";
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:0.25]}];

    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:1.0]}];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
    
    return YES;
}
@end
