//
//  AdditionalInvite.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 4/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "AdditionalInvite.h"

#import "cUITextField.h"

@interface AdditionalInvite () <UITextFieldDelegate>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) NSArray *dataArray;

@property (nonatomic, strong) NSNotificationCenter *keyboardNotification;

@end

@implementation AdditionalInvite

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
        
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
    }
    
    return self;
}

-(void)didMoveToSuperview{

    _salutationLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 40, _viewHeight)];
    _salutationLabel.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    _salutationLabel.text = @"Mr.";
    _salutationLabel.textAlignment = NSTextAlignmentCenter;
    _salutationLabel.font = [UIFont fontWithName:@"Futura ICG" size:13];
    _salutationLabel.textColor = [UIColor grayColor];
    [self addSubview:_salutationLabel];
    
    UIBezierPath *salutationMaskPath = [UIBezierPath bezierPathWithRoundedRect:_salutationLabel.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *salutationMaskLayer = [[CAShapeLayer alloc] init];
    salutationMaskLayer.frame = _salutationLabel.bounds;
    salutationMaskLayer.path  = salutationMaskPath.CGPath;
    _salutationLabel.layer.mask = salutationMaskLayer;
    
    UIView *arrowView = [[UIView alloc] initWithFrame:CGRectMake(_salutationLabel.frame.origin.x + _salutationLabel.frame.size.width, _salutationLabel.frame.origin.y, _viewHeight, _viewHeight)];
    arrowView.backgroundColor = [UIColor darkGrayColor];
    [self addSubview:arrowView];
    
    UIImageView *arrowDownImageView = [[UIImageView alloc] initWithFrame:CGRectMake((arrowView.frame.size.width / 2) - (arrowView.frame.size.width / 8), (arrowView.frame.size.height / 2) - (arrowView.frame.size.height / 8), arrowView.frame.size.width / 4, arrowView.frame.size.height / 4)];
    //arrowDownImageView.backgroundColor = [UIColor darkGrayColor];
    arrowDownImageView.image = [UIImage imageNamed:@"Down"];
    arrowDownImageView.clipsToBounds = YES;
    arrowDownImageView.contentMode = UIViewContentModeScaleAspectFill;
    [arrowView addSubview:arrowDownImageView];
    
    UIBezierPath *arrowMaskPath = [UIBezierPath bezierPathWithRoundedRect:arrowView.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *arrowMaskLayer = [[CAShapeLayer alloc] init];
    arrowMaskLayer.frame = arrowView.bounds;
    arrowMaskLayer.path  = arrowMaskPath.CGPath;
    arrowView.layer.mask = arrowMaskLayer;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(_salutationLabel.frame.origin.x, _salutationLabel.frame.origin.y,_salutationLabel.frame.origin.x + _salutationLabel.frame.size.width + arrowDownImageView.frame.size.width, _salutationLabel.frame.size.height);
    //button.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.4];
    [self addSubview:button];
    
    [button addTarget:self action:@selector(buttonEvent) forControlEvents:UIControlEventTouchUpInside];
    
    cUITextField *fullNameTextField = [[cUITextField alloc] initWithFrame:CGRectMake(button.frame.origin.x + button.frame.size.width + 10, button.frame.origin.y, _viewWidth - (button.frame.origin.x + button.frame.size.width + 10 + 20), button.frame.size.height)];
    fullNameTextField.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    //fullNameTextField.text = @"Name";
    fullNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    fullNameTextField.layer.cornerRadius = 5;
    fullNameTextField.delegate = self;
    fullNameTextField.font = [UIFont fontWithName:@"Futura ICG" size:13];
    fullNameTextField.textColor = [UIColor grayColor];
    [self addSubview:fullNameTextField];
    
    _invitedFullName = fullNameTextField.text;
    
    // single tap gesture recognizer
    //UITapGestureRecognizer *tapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDate:)];
    //tapGestureRecognize.numberOfTapsRequired = 1;
    //[self addGestureRecognizer:tapGestureRecognize];
    
    /*
     * Detect keyboard whether will show or hide
     * Used for screen to move up base on the height of keyboard
     * and so it doest overlap the keyboard over the email, password input and login button
     */
    
    _keyboardNotification = [NSNotificationCenter defaultCenter];
    
    [_keyboardNotification addObserver: self
                              selector: @selector(keyboardWillShow:)
                                  name: UIKeyboardWillChangeFrameNotification
                                object: nil];
    
    [_keyboardNotification addObserver: self
                              selector: @selector(keyboardWillHide:)
                                  name: UIKeyboardWillHideNotification
                                object: nil];
}



-(void)removeFromSuperview{

    _keyboardNotification = nil;
}

-(void)buttonEvent{

    NSLog(@"choose salutation");
    
    [_delegate AdditionalInviteDidPopUp:self];
}

/*
 * ==================== Delegates ====================
 */

-(void)keyboardWillShow:(NSNotification *)notification {
    
    [_delegate AdditionalKeyboardWillShow:notification];
    
    NSLog(@"keyboard will show");
}

-(void)keyboardWillHide:(NSNotification *)notification{
    
    [_delegate AdditionalKeyboardWillBeHidden:notification];
    
    NSLog(@"keyboard will hide");
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //textField.text = @"";
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor grayColor] colorWithAlphaComponent:0.25]}];
    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    _invitedFullName = textField.text;
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [[UIColor grayColor] colorWithAlphaComponent:1.0]}];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    _invitedFullName = textField.text;
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
