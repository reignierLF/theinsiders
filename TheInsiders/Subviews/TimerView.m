//
//  TimerView.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 6/2/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "TimerView.h"

#import "Gradient.h"

@interface TimerView ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) UILabel *timerLabel;

@property (nonatomic, strong) NSString *secToStr;
@property (nonatomic, strong) NSString *minToStr;
@property (nonatomic, strong) NSString *hrToStr;

//@property (nonatomic) NSInteger ms;
@property (nonatomic) NSInteger sec;
@property (nonatomic) NSInteger min;
@property (nonatomic) NSInteger hr;

@property (nonatomic) NSInteger totalInSec;

@property (nonatomic, strong) NSTimer *countDownTimer;

@end

@implementation TimerView

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
        
        _viewWidth = frame.size.width;
        _viewWidth = frame.size.height;
        
        _hr = 0;
        _min = 0;
        _sec = 5;
        
        NSInteger hrToSec = 3600 * _hr;
        NSInteger minToSec = 60 * _min;
        
        _totalInSec = hrToSec + minToSec + _sec;
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    [CATransaction begin];
    
    float radius = (_viewWidth / 4) + 10;
    float lineWidth = 15;
    float topMargin = 20;
    
    CAShapeLayer *whiteCircle = [CAShapeLayer layer];
    whiteCircle.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake((_viewWidth / 2) - (radius / 4), (_viewHeight / 4) + radius + lineWidth + topMargin) radius:radius startAngle:-M_PI_2 endAngle:2 * M_PI - M_PI_2 clockwise:YES].CGPath;
    whiteCircle.fillColor = [UIColor clearColor].CGColor;
    whiteCircle.strokeColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8].CGColor;
    whiteCircle.lineWidth = 2;
    
    CAShapeLayer *circle = [CAShapeLayer layer];
    circle.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake((_viewWidth / 2) - (radius / 4), (_viewHeight / 4) + radius + lineWidth + topMargin) radius:radius startAngle:-M_PI_2 endAngle:2 * M_PI - M_PI_2 clockwise:YES].CGPath;
    circle.fillColor = [UIColor clearColor].CGColor;
    circle.strokeColor = [Gradient greenColor].CGColor;
    circle.lineWidth = lineWidth;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = circle.mask.frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor cyanColor] CGColor], (id)[[UIColor purpleColor] CGColor], nil];
    [circle insertSublayer:gradient atIndex:0];
    
    NSLog(@"%f | %f", gradient.frame.size.width, gradient.frame.size.height);
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    animation.duration = _totalInSec;
    animation.removedOnCompletion = NO;
    animation.fromValue = @(0);
    animation.toValue = @(1);
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    [self.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    [self.layer addSublayer:whiteCircle];
    [self.layer addSublayer:circle];
    
    [CATransaction setCompletionBlock:^{
        
        [_countDownTimer invalidate];
        _countDownTimer = nil;
        
        _timerLabel.text = @"00:00:00";
        NSLog(@"animation done");
        
        [_delegate didFinishCountingDown:self];
    }];
    
    [circle addAnimation:animation forKey:@"drawCircleAnimation"];
    
    [CATransaction commit];
    
    _timerLabel = [[UILabel alloc] initWithFrame:CGRectMake((_viewWidth / 4) - ((radius / 2) - lineWidth), topMargin + lineWidth, radius * 2, radius * 2)];
    //timerLabel.backgroundColor = [UIColor yellowColor];
    _timerLabel.text = @"--:--:--";
    _timerLabel.textColor = [UIColor whiteColor];
    _timerLabel.font = [UIFont fontWithName:@"Futura ICG" size:35];
    _timerLabel.textAlignment = NSTextAlignmentCenter;
    _timerLabel.numberOfLines = 0;
    [self addSubview:_timerLabel];
    
    _countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
}

-(void)countDown{

    //_ms++;
    
    //if(_ms == 60){
    
    //    _ms = 0;
        _sec--;
    //}
    
    if(_sec < 0){
    
        _sec = 59;
        _min--;
    }
    
    if(_min < 0){
    
        _min = 59;
        _hr--;
    }
    
    if(_sec < 10){
        
        _secToStr = [NSString stringWithFormat:@"0%li",(long)_sec];
    }else{
        
        _secToStr = [NSString stringWithFormat:@"%li",(long)_sec];
    }
    
    if(_min < 10){
        
        _minToStr = [NSString stringWithFormat:@"0%li",(long)_min];
    }else{
        
        _minToStr = [NSString stringWithFormat:@"%li",(long)_min];
    }
    
    if(_hr < 10){
        
        _hrToStr = [NSString stringWithFormat:@"0%li",(long)_hr];
    }else{
        
        _hrToStr = [NSString stringWithFormat:@"%li",(long)_hr];
    }
    
    _timerLabel.text = [NSString stringWithFormat:@"%@:%@:%@",_hrToStr,_minToStr,_secToStr];
}

@end
