//
//  EventsCell.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 9/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EventsCell;

@protocol EventsCellDelegate

-(void)didClickCell:(EventsCell*)eventsCell sender:(UIButton*)sender;

@end

@interface EventsCell : UIView

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *dateTime;
@property (nonatomic, strong) NSString *eventImageUrl;
@property (nonatomic) BOOL hasInvited;
@property (nonatomic) BOOL isPastEvent;
@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *title;

@property (nonatomic, weak) id<EventsCellDelegate> delegate;

@end
