//
//  ListEvents.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 24/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "ListEvents.h"
#import "cUIScrollView.h"

#import "EventsCell.h"

@interface ListEvents () <UIScrollViewDelegate, EventsCellDelegate>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewheight;

@end

@implementation ListEvents

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewheight = frame.size.height;
        
        _size = 0;
        _isPastEvent = NO;
    }
    
    return self;
}

-(void)didMoveToSuperview{

    cUIScrollView *scrollview = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewheight)];
    //_scrollview.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
    scrollview.delegate = self;
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, (_viewheight / 3.5) * _size);
    scrollview.showsHorizontalScrollIndicator = NO;
    [self addSubview:scrollview];
    
    for (int i = 0; i < _size; i++) {
        
        EventsCell *eventsCell = [[EventsCell alloc] initWithFrame:CGRectMake(0, (_viewheight / 3.5) * i, scrollview.frame.size.width, _viewheight / 3.5)];
        eventsCell.delegate = self;
        eventsCell.alpha = 0.0;
        eventsCell.tag = i;
        eventsCell.address = [_address objectAtIndex:i];
        eventsCell.dateTime = [_dateTime objectAtIndex:i];
        eventsCell.eventImageUrl = [_eventImageUrl objectAtIndex:i];
        eventsCell.hasInvited = [[_hasInvited objectAtIndex:i] boolValue];
        eventsCell.eventId = [_eventId objectAtIndex:i];
        eventsCell.title = [_title objectAtIndex:i];
        eventsCell.isPastEvent = _isPastEvent;
        [scrollview addSubview:eventsCell];
        
        [UIView animateWithDuration: 0.5
                              delay: (i + 1) * 0.15
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations: ^{
                             
                             eventsCell.alpha = 1.0;
                         }completion:nil];
    }
}

-(void)didClickCell:(EventsCell *)eventsCell sender:(UIButton *)sender{
    
    [_delegate didSelectEvent:self sender:sender];
}
@end
