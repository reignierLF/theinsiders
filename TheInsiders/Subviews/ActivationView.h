//
//  ActivationView.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 12/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ActivationView;

@protocol ActivationDelegate

-(void)activate:(ActivationView*)activationView code:(NSString*)code;
-(void)activateWithLinkedIn:(ActivationView*)activationView code:(NSString*)code;

@end

@interface ActivationView : UIView

@property (nonatomic, weak) id <ActivationDelegate> delegate;

-(void)dismissAll;

@end
